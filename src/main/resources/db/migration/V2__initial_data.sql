-- password Admin123!
INSERT INTO ACCOUNTS values ('46396d43eda2454f866923d7e31fd6d5', 'admin@user.com', '$2a$10$mN3TZBTAOdS2YvU9nuMLc.T116TyaBrQ5Tax6Lr.lDcX3HH4KTJ8i');
INSERT INTO ROLES values ('46396d43eda2454f866923d7e31fd6d5', 'ADMIN');
INSERT INTO ACCOUNTS values ('736af199972040499ff928c0a1179930', 'designer@user.com', '$2a$10$mN3TZBTAOdS2YvU9nuMLc.T116TyaBrQ5Tax6Lr.lDcX3HH4KTJ8i');
INSERT INTO ROLES values ('736af199972040499ff928c0a1179930', 'DESIGNER');
INSERT INTO ACCOUNTS values ('c4e1547e3a2f4f9abb5124dd2352ffb1', 'lead-designer@user.com', '$2a$10$mN3TZBTAOdS2YvU9nuMLc.T116TyaBrQ5Tax6Lr.lDcX3HH4KTJ8i');
INSERT INTO ROLES values ('c4e1547e3a2f4f9abb5124dd2352ffb1', 'LEAD_DESIGNER');
INSERT INTO ACCOUNTS values ('b6964963780c429da5fc55d9184fc7f6', 'seller@user.com', '$2a$10$mN3TZBTAOdS2YvU9nuMLc.T116TyaBrQ5Tax6Lr.lDcX3HH4KTJ8i');
INSERT INTO ROLES values ('b6964963780c429da5fc55d9184fc7f6', 'SELLER');
INSERT INTO ACCOUNTS values ('541752cf26044d98a10e039120af3d63', 'storekeeper@user.com', '$2a$10$mN3TZBTAOdS2YvU9nuMLc.T116TyaBrQ5Tax6Lr.lDcX3HH4KTJ8i');
INSERT INTO ROLES values ('541752cf26044d98a10e039120af3d63', 'STOREKEEPER');
INSERT INTO ACCOUNTS values ('c50ee96346e64fdbaf18a1f10e856601', 'customer-support@user.com', '$2a$10$mN3TZBTAOdS2YvU9nuMLc.T116TyaBrQ5Tax6Lr.lDcX3HH4KTJ8i');
INSERT INTO ROLES values ('c50ee96346e64fdbaf18a1f10e856601', 'CUSTOMER_SUPPORT');
INSERT INTO ACCOUNTS values ('db19168c913f4085adb6efd07bdcf9f1', 'account-manager@user.com', '$2a$10$mN3TZBTAOdS2YvU9nuMLc.T116TyaBrQ5Tax6Lr.lDcX3HH4KTJ8i');
INSERT INTO ROLES values ('db19168c913f4085adb6efd07bdcf9f1', 'ACCOUNT_MANAGER');

INSERT INTO DESIGNS values ('604d3850db7048c19e3b552ea052dd24', '736af199972040499ff928c0a1179930', 'c4e1547e3a2f4f9abb5124dd2352ffb1', 'summer 2019 shirt', 'beach style', 'interesting cut', 'http://fashionhouse.com/design/shirt-2019.img', 'APPROVED', parsedatetime('17-04-2019 18:47:52.000', 'dd-MM-yyyy hh:mm:ss.SS'), null);

INSERT INTO CUSTOMERS values ('46396d43eda2454f866923d7e31fd6d5', 'admin@user.com', 'phone', 'firstName', 'lastName');
INSERT INTO CUSTOMER_ADDRESSES values ('0c3baddbea3e40c586fa79cbfa71e4b7', '46396d43eda2454f866923d7e31fd6d5', 'postcode', 'city', 'address', 'description');

INSERT INTO PRODUCTS values ('604d3850db7048c19e3b552ea052dd24', 69.99, 'shirt', 'slim shirt', 20, 'AVAILABLE');

INSERT INTO ITEMS values ('604d3850db7048c19e3b552ea052dd24', 'shirt', 'slim shirt', 'sector1', 'shelf1', 'bucket1', 20.0, 15.0, 5.0, 0.5);
INSERT INTO ITEM_EVENTS values ('38ba5a3f86364c9f9ab69a1f54b56cd4', 'ItemStockSupply', '604d3850db7048c19e3b552ea052dd24', parsedatetime('17-09-2019 18:47:52.000', 'dd-MM-yyyy hh:mm:ss.SS'), 15);
INSERT INTO ITEM_EVENTS values ('30d12df5571f4f5495bcaa52c4335c31', 'ItemStockSupply', '604d3850db7048c19e3b552ea052dd24', parsedatetime('11-10-2019 11:42:32.000', 'dd-MM-yyyy hh:mm:ss.SS'), 10);
INSERT INTO ITEM_EVENTS values ('5ad7a42c7c2f4d77a071fd049288967d', 'ItemStockSupply', '604d3850db7048c19e3b552ea052dd24', parsedatetime('01-11-2019 20:05:22.000', 'dd-MM-yyyy hh:mm:ss.SS'), 5);
