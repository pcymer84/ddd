package com.fashionhouse.account.repository;

import com.fashionhouse.account.ex.AccountNotFoundException;
import com.fashionhouse.account.model.Account;
import com.fashionhouse.core.logger.Log;
import java.util.UUID;

@Log
public interface CustomAccountRepository {

    Account getById(UUID userId) throws AccountNotFoundException;

}
