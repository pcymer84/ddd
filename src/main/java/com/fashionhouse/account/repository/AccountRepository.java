package com.fashionhouse.account.repository;

import com.fashionhouse.core.logger.Log;
import org.springframework.stereotype.Repository;

@Log
@Repository
public interface AccountRepository extends CrudAccountRepository, CustomAccountRepository {
}
