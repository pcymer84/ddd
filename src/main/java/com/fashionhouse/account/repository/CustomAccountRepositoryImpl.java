package com.fashionhouse.account.repository;

import com.fashionhouse.account.ex.AccountNotFoundException;
import com.fashionhouse.account.model.Account;
import lombok.AllArgsConstructor;
import java.util.UUID;

@AllArgsConstructor
public class CustomAccountRepositoryImpl implements CustomAccountRepository {
    
    private final CrudAccountRepository crudAccountRepository;

    @Override
    public Account getById(UUID userId) {
        return crudAccountRepository.findById(userId).orElseThrow(() -> new AccountNotFoundException(String.format("account with id '%s' not found", userId)));
    }

}
