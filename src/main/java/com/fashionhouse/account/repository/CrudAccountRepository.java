package com.fashionhouse.account.repository;

import com.fashionhouse.account.model.Account;
import com.fashionhouse.core.logger.Log;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log
@Repository
public interface CrudAccountRepository extends CrudRepository<Account, UUID> {

    Optional<Account> findByEmail(String email);

    @Override
    List<Account> findAll();

}
