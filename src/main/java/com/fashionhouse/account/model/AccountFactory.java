package com.fashionhouse.account.model;

import com.fashionhouse.account.dto.RegistrationRequest;
import com.fashionhouse.account.policy.EmailPolicy;
import com.fashionhouse.account.policy.PasswordPolicy;
import com.fashionhouse.core.annotations.Factory;
import lombok.AllArgsConstructor;

@Factory
@AllArgsConstructor
public class AccountFactory {

    private final EmailPolicy emailPolicy;
    private final PasswordPolicy passwordPolicy;

    public Account createForRegistration(RegistrationRequest request) {
        emailPolicy.validateEmailUsed(request.getEmail());
        passwordPolicy.validatePasswordStrength(request.getPassword());
        return Account.builder()
                .email(request.getEmail())
                .password(passwordPolicy.encodePassword(request.getPassword()))
                .build();
    }

}
