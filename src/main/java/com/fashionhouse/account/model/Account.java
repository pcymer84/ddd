package com.fashionhouse.account.model;

import com.fashionhouse.account.policy.EmailPolicy;
import com.fashionhouse.account.policy.PasswordPolicy;
import com.fashionhouse.core.annotations.AggregateRoot;
import com.fashionhouse.core.authentication.model.Role;
import com.fashionhouse.core.validator.ValidationUtil;
import com.google.common.collect.Sets;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@AggregateRoot
@Entity
@Builder
@Getter
@Table(name = "accounts")
@ToString(exclude = "password")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Account {

    @Id
    @NotNull
    private UUID id;
    @Email
    @NotNull
    private String email;
    private String password;
    @Getter(AccessLevel.NONE)
    @ElementCollection(targetClass = Role.class)
    @CollectionTable(name = "roles")
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

    public Set<Role> getRoles() {
        return Sets.newHashSet(roles);
    }

    public void assignRole(Role role) {
        roles.add(role);
    }

    public void removeRole(Role role) {
        roles.remove(role);
    }

    public void changeEmail(String email, EmailPolicy emailPolicy) {
        emailPolicy.validateEmailUsed(id, email);
        this.email = email;
    }

    public void changePassword(String newPassword, PasswordPolicy passwordPolicy) {
        passwordPolicy.validatePasswordStrength(newPassword);
        this.password = passwordPolicy.encodePassword(newPassword);
    }

    private Account(Account.AccountBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        email = builder.email;
        password = builder.password;
        roles = CollectionUtils.isEmpty(builder.roles) ? Sets.newHashSet() :  builder.roles;
        ValidationUtil.validate(this);
    }

    static class AccountBuilder {

        Account build() {
            return new Account(this);
        }

    }

}
