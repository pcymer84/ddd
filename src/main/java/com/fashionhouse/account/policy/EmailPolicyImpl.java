package com.fashionhouse.account.policy;

import com.fashionhouse.account.ex.EmailAlreadyUsedException;
import com.fashionhouse.account.ex.AccountAlreadyUsingEmailException;
import com.fashionhouse.account.model.Account;
import com.fashionhouse.account.repository.AccountRepository;
import com.fashionhouse.core.annotations.Policy;
import lombok.AllArgsConstructor;
import java.util.Optional;
import java.util.UUID;

@Policy
@AllArgsConstructor
public class EmailPolicyImpl implements EmailPolicy {

    private final AccountRepository accountRepository;

    @Override
    public void validateEmailUsed(String email) {
        accountRepository.findByEmail(email)
                .ifPresent(account -> { throw new AccountAlreadyUsingEmailException(String.format("account with email '%s' already exists", email)); });
    }

    @Override
    public void validateEmailUsed(UUID accountId, String email) {
        Optional<Account> foundAccount = accountRepository.findByEmail(email);
        if (foundAccount.isPresent()) {
            Account account = foundAccount.get();
            if (account.getEmail().equals(email) && account.getId().equals(accountId)) {
                throw new EmailAlreadyUsedException(String.format("account with id '%s' is already using email '%s'", accountId, email));
            }
            throw new AccountAlreadyUsingEmailException(String.format("account with email '%s' already exists", email));
        }
    }

}
