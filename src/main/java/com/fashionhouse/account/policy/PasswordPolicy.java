package com.fashionhouse.account.policy;

public interface PasswordPolicy {

    void validatePasswordStrength(String rawPassword);

    String encodePassword(String rawPassword);

}
