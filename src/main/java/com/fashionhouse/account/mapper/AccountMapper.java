package com.fashionhouse.account.mapper;

import com.fashionhouse.account.dto.AccountResponse;
import com.fashionhouse.account.model.Account;
import org.mapstruct.Mapper;

@Mapper
public interface AccountMapper {

    AccountResponse mapToAccountResponse(Account account);

}
