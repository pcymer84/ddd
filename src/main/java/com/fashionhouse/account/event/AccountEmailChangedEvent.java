package com.fashionhouse.account.event;

import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@ToString
@Value
public class AccountEmailChangedEvent {

    private UUID accountId;
    private String email;

}
