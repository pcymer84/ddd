package com.fashionhouse.account.endpoint;

import com.fashionhouse.account.dto.ChangeEmailRequest;
import com.fashionhouse.account.dto.ChangePasswordRequest;
import com.fashionhouse.account.facade.AccountManagementFacade;
import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.Role;
import com.fashionhouse.core.authentication.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class AccountManagementEndpoint {

    private final AccountManagementFacade accountManagementFacade;

    @PutMapping("/accounts/me/change_email")
    public void changeMyEmail(@AuthenticationPrincipal User user,
                              @Valid @RequestBody ChangeEmailRequest request) {
        accountManagementFacade.changeMyEmail(user, request);
    }

    @PutMapping("/accounts/me/change_password")
    public void changeMyPassword(@AuthenticationPrincipal User user,
                                 @Valid @RequestBody ChangePasswordRequest request) {
        accountManagementFacade.changeMyPassword(user, request);
    }

    @PutMapping("/accounts/{accountId}/change_email")
    public void changeEmail(@AuthenticationPrincipal User user,
                            @PathVariable("accountId") UUID accountId,
                            @Valid @RequestBody ChangeEmailRequest request) {
        accountManagementFacade.changeEmail(user, accountId, request);
    }

    @PutMapping("/accounts/{accountId}/change_password")
    public void changePassword(@AuthenticationPrincipal User user,
                               @PathVariable("accountId") UUID accountId,
                               @Valid @RequestBody ChangePasswordRequest request) {
        accountManagementFacade.changePassword(user, accountId, request);
    }

    @PutMapping("/accounts/{accountId}/roles/{role}")
    public void assignRole(@AuthenticationPrincipal User user,
                           @PathVariable("accountId") UUID accountId,
                           @PathVariable("role") Role role) {
        accountManagementFacade.assigneeRole(user, accountId, role);
    }

    @DeleteMapping("/accounts/{accountId}/roles/{role}")
    public void removeRole(@AuthenticationPrincipal User user,
                           @PathVariable("accountId") UUID accountId,
                           @PathVariable("role") Role role) {
        accountManagementFacade.removeRole(user, accountId, role);
    }

    @DeleteMapping("/accounts/{accountId}")
    public void deleteAccount(@AuthenticationPrincipal User user,
                              @PathVariable("accountId") UUID accountId) {
        accountManagementFacade.deleteAccount(user, accountId);
    }

}
