package com.fashionhouse.account.endpoint;

import com.fashionhouse.account.dto.AccountResponse;
import com.fashionhouse.account.dto.AuthenticateRequest;
import com.fashionhouse.account.dto.RegistrationRequest;
import com.fashionhouse.account.facade.AccountFacade;
import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;

@ApiV1
@Endpoint
@AllArgsConstructor
public class AccountEndpoint {

    private final AccountFacade accountFacade;

    @PostMapping("/accounts/authenticate")
    public String authenticate(@Valid @RequestBody AuthenticateRequest request) {
        return accountFacade.authenticate(request);
    }

    @PostMapping("/accounts/register")
    public AccountResponse register(@Valid @RequestBody RegistrationRequest request) {
        return accountFacade.register(request);
    }

}
