package com.fashionhouse.account.endpoint;

import com.fashionhouse.account.dto.AccountResponse;
import com.fashionhouse.account.facade.AccountQueryFacade;
import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class AccountQueryEndpoint {

    private final AccountQueryFacade accountQueryFacade;

    @GetMapping("/accounts/me")
    public AccountResponse getMyAccount(@AuthenticationPrincipal User user) {
        return accountQueryFacade.getMyAccount(user);
    }

    @GetMapping("/accounts/{accountId}")
    public AccountResponse getAccount(@AuthenticationPrincipal User user,
                                      @PathVariable("accountId") UUID accountId) {
        return accountQueryFacade.getAccount(user, accountId);
    }

    @GetMapping("/accounts")
    public List<AccountResponse> getAccounts(@AuthenticationPrincipal User user) {
        return accountQueryFacade.getAccounts(user);
    }

}
