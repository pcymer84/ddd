package com.fashionhouse.account.ex;

import com.fashionhouse.core.ex.ClientSideException;

public class AccountAlreadyUsingEmailException extends ClientSideException {

    public AccountAlreadyUsingEmailException(String message) {
        super(message);
    }

}
