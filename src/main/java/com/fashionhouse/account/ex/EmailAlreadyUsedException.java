package com.fashionhouse.account.ex;

import com.fashionhouse.core.ex.ClientSideException;

public class EmailAlreadyUsedException extends ClientSideException {

    public EmailAlreadyUsedException(String message) {
        super(message);
    }

}
