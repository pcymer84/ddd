package com.fashionhouse.account.ex;

import com.fashionhouse.core.ex.NotFoundException;

public class AccountNotFoundException extends NotFoundException {

    public AccountNotFoundException(String message) {
        super(message);
    }

}
