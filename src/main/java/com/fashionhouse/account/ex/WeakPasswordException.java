package com.fashionhouse.account.ex;

import com.fashionhouse.core.ex.ClientSideException;

public class WeakPasswordException extends ClientSideException {

    public WeakPasswordException(String message) {
        super(message);
    }

}
