package com.fashionhouse.account.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@ToString(exclude = "confirmingPassword")
@NoArgsConstructor
public class ChangeEmailRequest {

    @NotNull
    @Email
    private String email;
    @NotNull
    private String confirmingPassword;

}
