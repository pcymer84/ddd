package com.fashionhouse.account.dto;

import com.fashionhouse.core.authentication.model.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountResponse {

    private UUID id;
    private String email;
    private Set<Role> roles;

}
