package com.fashionhouse.account.dto;

import com.fashionhouse.core.validator.annotations.Password;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@ToString(exclude = "password")
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationRequest {

    @NotNull
    @Email
    private String email;
    @NotNull
    @Password
    private String password;

}
