package com.fashionhouse.account.dto;

import com.fashionhouse.core.validator.annotations.Password;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.NotNull;

@Data
@ToString(exclude = {"confirmingPassword", "newPassword"})
@NoArgsConstructor
@AllArgsConstructor
public class ChangePasswordRequest {

    @NotNull
    @Password
    private String newPassword;
    @NotNull
    private String confirmingPassword;

}
