package com.fashionhouse.account.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.NotNull;

@Data
@ToString(exclude = "password")
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticateRequest {

    @NotNull
    private String email;
    @NotNull
    private String password;

}
