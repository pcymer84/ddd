package com.fashionhouse.account.facade;

import com.fashionhouse.account.dto.AccountResponse;
import com.fashionhouse.account.mapper.AccountMapper;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.account.repository.AccountRepository;
import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.User;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationService
@AllArgsConstructor
public class AccountQueryFacadeImpl implements AccountQueryFacade {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    @Override
    public AccountResponse getMyAccount(User user) {
        return accountMapper.mapToAccountResponse(accountRepository.getById(user.getId()));
    }

    @Override
    public AccountResponse getAccount(User user, UUID accountId) {
        user.authorize(Permission.SHOW_ACCOUNT);
        return accountMapper.mapToAccountResponse(accountRepository.getById(accountId));
    }

    @Override
    public List<AccountResponse> getAccounts(User user) {
        user.authorize(Permission.SHOW_ACCOUNT);
        return accountRepository.findAll().stream()
                .map(accountMapper::mapToAccountResponse)
                .collect(Collectors.toList());
    }

}
