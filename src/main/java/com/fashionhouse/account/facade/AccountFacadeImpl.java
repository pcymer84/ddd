package com.fashionhouse.account.facade;

import com.fashionhouse.account.dto.AccountResponse;
import com.fashionhouse.account.dto.AuthenticateRequest;
import com.fashionhouse.account.dto.RegistrationRequest;
import com.fashionhouse.account.event.AccountCreatedEvent;
import com.fashionhouse.account.mapper.AccountMapper;
import com.fashionhouse.account.model.Account;
import com.fashionhouse.account.model.AccountFactory;
import com.fashionhouse.account.repository.AccountRepository;
import com.fashionhouse.account.service.AuthenticationService;
import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.eventBus.EventPublisher;
import lombok.AllArgsConstructor;

@ApplicationService
@AllArgsConstructor
public class AccountFacadeImpl implements AccountFacade {

    private final AccountFactory accountFactory;
    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;
    private final AuthenticationService authenticationService;
    private final EventPublisher eventPublisher;

    @Override
    public String authenticate(AuthenticateRequest request) {
        return authenticationService.authenticate(request.getEmail(), request.getPassword());
    }

    @Override
    public AccountResponse register(RegistrationRequest request) {
        Account account = accountFactory.createForRegistration(request);
        accountRepository.save(account);
        eventPublisher.publishEvent(new AccountCreatedEvent(account.getId(), account.getEmail()));
        return accountMapper.mapToAccountResponse(account);
    }

}
