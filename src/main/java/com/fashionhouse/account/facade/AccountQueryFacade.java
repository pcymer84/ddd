package com.fashionhouse.account.facade;

import com.fashionhouse.account.dto.AccountResponse;
import com.fashionhouse.core.authentication.model.User;
import java.util.List;
import java.util.UUID;

public interface AccountQueryFacade {

    AccountResponse getMyAccount(User user);

    AccountResponse getAccount(User user, UUID accountId);

    List<AccountResponse> getAccounts(User user);

}
