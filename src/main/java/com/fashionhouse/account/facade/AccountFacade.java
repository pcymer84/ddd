package com.fashionhouse.account.facade;

import com.fashionhouse.account.dto.AccountResponse;
import com.fashionhouse.account.dto.AuthenticateRequest;
import com.fashionhouse.account.dto.RegistrationRequest;

public interface AccountFacade {

    String authenticate(AuthenticateRequest request);

    AccountResponse register(RegistrationRequest request);

}
