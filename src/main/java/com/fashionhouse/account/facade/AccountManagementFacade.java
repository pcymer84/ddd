package com.fashionhouse.account.facade;

import com.fashionhouse.account.dto.ChangeEmailRequest;
import com.fashionhouse.account.dto.ChangePasswordRequest;
import com.fashionhouse.core.authentication.model.Role;
import com.fashionhouse.core.authentication.model.User;
import java.util.UUID;

public interface AccountManagementFacade {

    void changeMyEmail(User user, ChangeEmailRequest request);

    void changeMyPassword(User user, ChangePasswordRequest request);

    void changeEmail(User user, UUID accountId, ChangeEmailRequest request);

    void changePassword(User user, UUID accountId, ChangePasswordRequest request);

    void assigneeRole(User user, UUID accountId, Role role);

    void removeRole(User user, UUID accountId, Role role);

    void deleteAccount(User user, UUID accountId);

}
