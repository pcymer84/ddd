package com.fashionhouse.account.facade;

import com.fashionhouse.account.dto.ChangeEmailRequest;
import com.fashionhouse.account.dto.ChangePasswordRequest;
import com.fashionhouse.account.event.AccountEmailChangedEvent;
import com.fashionhouse.account.model.Account;
import com.fashionhouse.account.policy.EmailPolicy;
import com.fashionhouse.account.policy.PasswordPolicy;
import com.fashionhouse.account.repository.AccountRepository;
import com.fashionhouse.account.service.AuthenticationService;
import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.Role;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.core.eventBus.EventPublisher;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class AccountManagementFacadeImpl implements AccountManagementFacade {

    private final AccountRepository accountRepository;
    private final PasswordPolicy passwordPolicy;
    private final EmailPolicy emailPolicy;
    private final AuthenticationService authenticationService;
    private final EventPublisher eventPublisher;

    @Override
    public void changeMyEmail(User user, ChangeEmailRequest request) {
        authenticationService.authenticate(user.getUsername(), request.getConfirmingPassword());
        Account account = accountRepository.getById(user.getId());
        account.changeEmail(request.getEmail(), emailPolicy);
        accountRepository.save(account);
        eventPublisher.publishEvent(new AccountEmailChangedEvent(account.getId(), account.getEmail()));
    }

    @Override
    public void changeMyPassword(User user, ChangePasswordRequest request) {
        authenticationService.authenticate(user.getUsername(), request.getConfirmingPassword());
        Account account = accountRepository.getById(user.getId());
        account.changePassword(request.getNewPassword(), passwordPolicy);
        accountRepository.save(account);
    }

    @Override
    public void changeEmail(User user, UUID accountId, ChangeEmailRequest request) {
        user.authorize(Permission.CHANGE_EMAIL);
        authenticationService.authenticate(user.getUsername(), request.getConfirmingPassword());
        Account account = accountRepository.getById(accountId);
        account.changeEmail(request.getEmail(), emailPolicy);
        accountRepository.save(account);
        eventPublisher.publishEvent(new AccountEmailChangedEvent(account.getId(), account.getEmail()));
    }

    @Override
    public void changePassword(User user, UUID accountId, ChangePasswordRequest request) {
        user.authorize(Permission.CHANGE_PASSWORD);
        authenticationService.authenticate(user.getUsername(), request.getConfirmingPassword());
        Account account = accountRepository.getById(accountId);
        account.changePassword(request.getNewPassword(), passwordPolicy);
        accountRepository.save(account);
    }

    @Override
    public void assigneeRole(User user, UUID accountId, Role role) {
        user.authorize(Permission.ROLE_MANAGEMENT);
        Account account = accountRepository.getById(accountId);
        account.assignRole(role);
        accountRepository.save(account);
    }

    @Override
    public void removeRole(User user, UUID accountId, Role role) {
        user.authorize(Permission.ROLE_MANAGEMENT);
        Account account = accountRepository.getById(accountId);
        account.removeRole(role);
        accountRepository.save(account);
    }

    @Override
    public void deleteAccount(User user, UUID accountId) {
        user.authorize(Permission.DELETE_ACCOUNT);
        accountRepository.deleteById(accountId);
    }

}
