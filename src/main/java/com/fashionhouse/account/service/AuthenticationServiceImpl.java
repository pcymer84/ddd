package com.fashionhouse.account.service;

import com.fashionhouse.core.annotations.InfrastructureService;
import com.fashionhouse.core.authentication.ex.AuthenticationException;
import com.fashionhouse.core.authentication.factory.AuthenticationFactory;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;

@InfrastructureService
@AllArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final AuthenticationFactory authenticationFactory;
    private final TokenFactory tokenFactory;

    @Override
    public String authenticate(String email, String password) {
        try {
            Authentication authentication = authenticationManager.authenticate(authenticationFactory.createAuthentication(email, password));
            return tokenFactory.createFromAuthentication(authentication);
        } catch (org.springframework.security.core.AuthenticationException e) {
            throw new AuthenticationException("invalid login or password", e);
        }
    }

}
