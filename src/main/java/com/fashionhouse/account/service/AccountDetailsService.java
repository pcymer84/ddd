package com.fashionhouse.account.service;

import com.fashionhouse.account.repository.AccountRepository;
import com.fashionhouse.core.annotations.InfrastructureService;
import com.fashionhouse.core.authentication.factory.UserFactory;
import com.fashionhouse.core.authentication.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@InfrastructureService
@AllArgsConstructor
public class AccountDetailsService implements UserDetailsService {

    private final AccountRepository accountRepository;
    private final UserFactory userFactory;

    @Override
    public User loadUserByUsername(String email) throws UsernameNotFoundException {
        return accountRepository.findByEmail(email)
                .map(userFactory::createFromAccount)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("user with email '%s' not found", email)));
    }

}
