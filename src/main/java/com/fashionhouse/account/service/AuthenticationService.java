package com.fashionhouse.account.service;

public interface AuthenticationService {

    String authenticate(String login, String password);

}
