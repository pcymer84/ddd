package com.fashionhouse.core.config;

import com.google.common.eventbus.EventBus;
import com.fashionhouse.core.eventBus.EventSubscriber;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Configuration
@AllArgsConstructor
public class StartupConfig {

    private final ApplicationContext applicationContext;
    private final EventBus eventBus;

    @EventListener(ContextRefreshedEvent.class)
    public void setupEventSubscribers() {
        applicationContext.getBeansWithAnnotation(EventSubscriber.class).values().forEach(eventBus::register);
    }

}
