package com.fashionhouse.core.config;

import com.fashionhouse.core.authentication.JwtSecurityProperties;
import com.fashionhouse.core.authentication.AuthenticationFilter;
import com.fashionhouse.core.authentication.TokenAuthenticationProvider;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import javax.servlet.http.HttpServletResponse;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final JwtSecurityProperties jwtSecurityProperties;
    private final TokenAuthenticationProvider tokenAuthenticationProvider;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().headers().frameOptions().sameOrigin()
                .and().csrf().disable().cors()
                .and().exceptionHandling().authenticationEntryPoint((req, res, e) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                .and().addFilterAfter(createAuthenticationFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests()
                    .antMatchers(unauthenticatedAddresses()).permitAll()
                    .antMatchers(unauthenticatedEndpoints()).permitAll()
                    .anyRequest().authenticated();
    }

    private String[] unauthenticatedAddresses() {
        return jwtSecurityProperties.getUnauthenticatedAddresses().toArray(new String[0]);
    }

    private String[] unauthenticatedEndpoints() {
        return jwtSecurityProperties.getUnauthenticatedEndpoints().stream()
                .map(this::api)
                .toArray(String[]::new);
    }

    private String api(String endpoint) {
        return "/api/v{\\d+}" + endpoint;
    }

    private AuthenticationFilter createAuthenticationFilter() {
        return new AuthenticationFilter(jwtSecurityProperties, tokenAuthenticationProvider);
    }

}
