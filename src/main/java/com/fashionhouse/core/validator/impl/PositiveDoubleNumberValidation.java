package com.fashionhouse.core.validator.impl;

import com.fashionhouse.core.validator.annotations.PositiveNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PositiveDoubleNumberValidation implements ConstraintValidator<PositiveNumber, Double> {

    @Override
    public boolean isValid(Double value, ConstraintValidatorContext context) {
        return value == null || value > 0;
    }

}
