package com.fashionhouse.core.validator.impl;

import com.fashionhouse.core.validator.annotations.URL;
import org.apache.commons.validator.routines.UrlValidator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UrlValidation implements ConstraintValidator<URL, String> {

    private static final UrlValidator urlValidator = new UrlValidator();

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || urlValidator.isValid(value);
    }

}
