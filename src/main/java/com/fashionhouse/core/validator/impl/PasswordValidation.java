package com.fashionhouse.core.validator.impl;

import com.google.common.base.Joiner;
import com.fashionhouse.account.ex.WeakPasswordException;
import com.fashionhouse.core.validator.annotations.Password;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.passay.WhitespaceRule;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class PasswordValidation implements ConstraintValidator<Password, String> {

    private static final PasswordValidator validator = createPasswordValidator();

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        if (password == null) {
            return true;
        }
        RuleResult result = validator.validate(new PasswordData(password));
        if (result.isValid()) {
            return true;
        }
        if (context != null) {
            context.buildConstraintViolationWithTemplate(createValidationMessage(result))
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }
        return false;
    }

    public void validatePassword(String password) {
        if (password != null) {
            RuleResult result = validationResult(password);
            if (!result.isValid()) {
                throw new WeakPasswordException(createValidationMessage(result));
            }
        }
    }

    private RuleResult validationResult(String password) {
        return validator.validate(new PasswordData(password));
    }

    private String createValidationMessage(RuleResult result) {
        return Joiner.on(" ").join(validator.getMessages(result));
    }

    private static PasswordValidator createPasswordValidator() {
        return new PasswordValidator(Arrays.asList(
                new LengthRule(8, 30),
                new CharacterRule(EnglishCharacterData.Digit, 1),
                new CharacterRule(EnglishCharacterData.LowerCase, 1),
                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                new CharacterRule(EnglishCharacterData.Special, 1),
                new WhitespaceRule()));
    }

}
