package com.fashionhouse.core.validator.impl;

import com.fashionhouse.core.validator.annotations.Month;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MonthValidation implements ConstraintValidator<Month, Number> {

    @Override
    public boolean isValid(Number value, ConstraintValidatorContext context) {
        return value == null || validate(value);
    }

    private boolean validate(Number value) {
        int val = value.intValue();
        return val >= 1 && val <= 12;
    }

}
