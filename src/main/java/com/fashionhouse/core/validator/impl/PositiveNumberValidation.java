package com.fashionhouse.core.validator.impl;

import com.fashionhouse.core.validator.annotations.PositiveNumber;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PositiveNumberValidation implements ConstraintValidator<PositiveNumber, Long> {

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return value == null || value > 0;
    }

}
