package com.fashionhouse.core.validator.impl;

import com.fashionhouse.core.validator.annotations.Year;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class YearValidation implements ConstraintValidator<Year, Number> {

    private static final Integer MIN = 1970;
    private static final Integer MAX = 2300;

    @Override
    public boolean isValid(Number value, ConstraintValidatorContext context) {
        return value == null || validate(value);
    }

    private boolean validate(Number value) {
        int val = value.intValue();
        return val >= MIN && val <= MAX;
    }

}
