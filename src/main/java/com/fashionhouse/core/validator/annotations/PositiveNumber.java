package com.fashionhouse.core.validator.annotations;

import com.fashionhouse.core.validator.impl.PositiveDoubleNumberValidation;
import com.fashionhouse.core.validator.impl.PositiveNumberValidation;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = {PositiveNumberValidation.class, PositiveDoubleNumberValidation.class})
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PositiveNumber {

    String message() default "Negative number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
