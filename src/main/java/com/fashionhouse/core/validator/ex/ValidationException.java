package com.fashionhouse.core.validator.ex;

import com.fashionhouse.core.ex.ClientSideException;
import lombok.Getter;
import org.springframework.validation.BindingResult;

@Getter
public class ValidationException extends ClientSideException {

    private BindingResult bindingResult;

    public ValidationException(String message, BindingResult bindingResult) {
        super(message);
        this.bindingResult = bindingResult;
    }

}