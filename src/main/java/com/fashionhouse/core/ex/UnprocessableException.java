package com.fashionhouse.core.ex;

public class UnprocessableException extends ClientSideException {

    public UnprocessableException(String message) {
        super(message);
    }

}
