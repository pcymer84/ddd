package com.fashionhouse.core.authentication.ex;

import com.fashionhouse.core.ex.ClientSideException;

public class AuthorizationException extends ClientSideException {

    public AuthorizationException(String message) {
        super(message);
    }

}
