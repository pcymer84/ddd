package com.fashionhouse.core.authentication.ex;

import com.fashionhouse.core.ex.ClientSideException;

public class AuthenticationException extends ClientSideException {

    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
