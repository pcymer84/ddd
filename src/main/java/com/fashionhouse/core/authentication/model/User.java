package com.fashionhouse.core.authentication.model;

import com.fashionhouse.core.authentication.ex.AuthorizationException;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.UUID;

@ToString(exclude = {"password"})
@Slf4j
@Builder
@Getter
public class User implements UserDetails {

    private UUID id;
    private String username;
    private String password;
    private boolean disabled;
    private boolean accountExpired;
    private boolean credentialsExpired;
    private boolean accountLocked;
    private Collection<Authority> authorities;

    @Override
    public boolean isAccountNonExpired() {
        return !accountExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !accountLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !credentialsExpired;
    }

    @Override
    public boolean isEnabled() {
        return !disabled;
    }

    public void authorize(Permission permission) {
        log.debug("authorize permission '{}'", permission);
        if (!isAuthorized(permission)) {
            throw new AuthorizationException(String.format("permission '%s' not granted", permission));
        }
        log.debug("permission '{}' granted", permission);
    }

    public boolean isAuthorized(Permission permission) {
        return authorities.stream().anyMatch(authority -> authority.hasPermission(permission));
    }

}
