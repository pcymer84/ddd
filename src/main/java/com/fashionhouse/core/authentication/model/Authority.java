package com.fashionhouse.core.authentication.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;

@EqualsAndHashCode
@ToString
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Authority implements GrantedAuthority {

    private Role role;

    @Override
    public String getAuthority() {
        return getRole().name();
    }

    public boolean hasPermission(Permission permission) {
        return role.hasPermission(permission);
    }

}
