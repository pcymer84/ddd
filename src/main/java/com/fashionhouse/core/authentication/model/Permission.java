package com.fashionhouse.core.authentication.model;

public enum Permission {

    SHOW_ACCOUNT,
    CHANGE_EMAIL,
    CHANGE_PASSWORD,
    ROLE_MANAGEMENT,
    DELETE_ACCOUNT,

    SHOW_DESIGN,
    MAKE_DESIGN,
    VERIFY_DESIGN,

    PRODUCT_MANAGEMENT,

    SHOW_CUSTOMERS,
    SHOW_ORDERS,
    ORDER_MANAGEMENT,

    SHOW_ITEM,
    ITEM_MANAGER,
    ITEM_SUPPLIER,
    SHOW_PARCEL,
    PARCEL_SENDER,

}
