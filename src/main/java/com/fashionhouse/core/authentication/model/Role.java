package com.fashionhouse.core.authentication.model;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.List;

@Getter
@AllArgsConstructor
public enum Role {

    ADMIN(
            Lists.newArrayList(Permission.values())
    ),
    DESIGNER(
            Lists.newArrayList(
                    Permission.SHOW_DESIGN,
                    Permission.MAKE_DESIGN
            )
    ),
    LEAD_DESIGNER(
            Lists.newArrayList(
                    Permission.SHOW_DESIGN,
                    Permission.MAKE_DESIGN,
                    Permission.VERIFY_DESIGN
            )
    ),
    SELLER(
            Lists.newArrayList(
                    Permission.PRODUCT_MANAGEMENT
            )
    ),
    STOREKEEPER(
            Lists.newArrayList(
                    Permission.SHOW_ITEM,
                    Permission.ITEM_MANAGER,
                    Permission.ITEM_SUPPLIER,
                    Permission.SHOW_PARCEL,
                    Permission.PARCEL_SENDER
            )
    ),
    CUSTOMER_SUPPORT(
            Lists.newArrayList(
                    Permission.SHOW_CUSTOMERS,
                    Permission.SHOW_ORDERS,
                    Permission.SHOW_ITEM,
                    Permission.SHOW_PARCEL,
                    Permission.ORDER_MANAGEMENT
            )
    ),
    ACCOUNT_MANAGER(
            Lists.newArrayList(
                    Permission.SHOW_ACCOUNT,
                    Permission.CHANGE_EMAIL,
                    Permission.CHANGE_PASSWORD,
                    Permission.ROLE_MANAGEMENT,
                    Permission.DELETE_ACCOUNT
            )
    );

    private List<Permission> permissions;

    public boolean hasPermission(Permission permission) {
        return permissions.stream().anyMatch(p -> p == permission);
    }

}
