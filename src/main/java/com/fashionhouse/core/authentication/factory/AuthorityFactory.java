package com.fashionhouse.core.authentication.factory;

import com.fashionhouse.account.model.Account;
import com.fashionhouse.core.authentication.model.Authority;
import com.fashionhouse.account.service.TokenFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class AuthorityFactory {

    private final ObjectMapper objectMapper;
    private final TokenFactory tokenFactory;

    public List<Authority> createAuthorities(Account account) {
        return account.getRoles().stream()
                .map(Authority::new)
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<Authority> createAuthorities(Claims claims) {
        return objectMapper.readValue(
                objectMapper.writeValueAsString(claims.get(tokenFactory.tokenPermissionsKey())),
                new TypeReference<>(){});
    }

}
