package com.fashionhouse.core.authentication.factory;

import com.fashionhouse.core.authentication.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AuthenticationFactory {

    public Authentication createAuthentication(String login, String password) {
        return new UsernamePasswordAuthenticationToken(login, password, null);
    }

    public Authentication createAuthentication(User user) {
        return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
    }

}
