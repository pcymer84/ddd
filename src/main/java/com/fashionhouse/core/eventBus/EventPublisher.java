package com.fashionhouse.core.eventBus;

public interface EventPublisher {

    void publishEvent(Object event);

}
