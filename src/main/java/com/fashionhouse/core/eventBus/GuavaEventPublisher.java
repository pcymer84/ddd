package com.fashionhouse.core.eventBus;

import com.google.common.eventbus.EventBus;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class GuavaEventPublisher implements EventPublisher {

    private final EventBus eventBus;

    @Override
    public void publishEvent(Object event) {
        eventBus.post(event);
        log.debug("publish event : {}", event);
    }

}
