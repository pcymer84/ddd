package com.fashionhouse.core.logger;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import java.util.Arrays;
import java.util.Collection;

@Component
public class LogTemplate {

    private static final String CONTENT = "'%s'";
    private static final String LIMITED_CONTENT = "'%s(...)' characters=%d";
    private static final String COLLECTION = " items=%d";
    private static final String TIMER = " time=%sms";
    private static final String TEMPLATE = "%s %s %s %s";
    private static final String TEMPLATE_WITH_TIMER = TEMPLATE + TIMER;
    private static final int CHARACTERS_LIMIT = 300;


    public String createLogInMessage(LogModel logModel) {
        return String.format(TEMPLATE,
                logModel.getClassName(),
                logModel.getMethodName(),
                ">>",
                Arrays.toString(logModel.getArgs())
        );
    }

    public String createLogOutMessage(LogModel logModel) {
       return String.format(outFormat(logModel),
               logModel.getClassName(),
               logModel.getMethodName(),
               "<<",
               createContent(logModel),
               logModel.calculateTime());
    }

    private String createContent(LogModel logModel) {
        return isVoid(logModel.getMethodReturnType()) ? "void" : createResultContent(logModel);
    }

    private String createResultContent(LogModel logModel) {
        return logModel.getResult() == null ? "null" : createResultContentMessage(logModel);
    }

    private String createResultContentMessage(LogModel logModel) {
        Object result = logModel.getResult();
        return result instanceof Collection ?
                createObjectMessage(logModel) + String.format(COLLECTION, ((Collection<?>) result).size()) :
                createObjectMessage(logModel);
    }

    private String createObjectMessage(LogModel logModel) {
        String objectMessage = logModel.getResult().toString();
        return shouldLogFullContent(logModel, objectMessage) ?
                String.format(CONTENT, objectMessage) :
                String.format(LIMITED_CONTENT, StringUtils.left(objectMessage, CHARACTERS_LIMIT), objectMessage.length());
    }

    private <T> boolean shouldLogFullContent(LogModel logModel, String result) {
        return logModel.isLimitContent() && result.length() <= CHARACTERS_LIMIT;
    }

    private String outFormat(LogModel logModel) {
        return logModel.isLogTime() ? TEMPLATE_WITH_TIMER : TEMPLATE;
    }

    private boolean isVoid(Class clazz) {
        return clazz == void.class || clazz == Void.class;
    }

}
