package com.fashionhouse.core.logger;

public enum LogLevel {

    DEBUG,
    INFO,
    WARN,
    ERROR

}
