package com.fashionhouse.core.logger;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {

    LogLevel level() default LogLevel.DEBUG;

    boolean logTime() default false;

    boolean limitContent() default true;

}
