package com.fashionhouse.core.annotations;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.core.logger.LogLevel;
import org.springframework.web.bind.annotation.RestController;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Log(level = LogLevel.INFO, logTime = true)
@RestController
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Endpoint {
}
