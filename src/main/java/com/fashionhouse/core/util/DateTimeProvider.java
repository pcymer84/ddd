package com.fashionhouse.core.util;

import lombok.experimental.UtilityClass;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@UtilityClass
public class DateTimeProvider {

    public static final ZoneId ZONE = ZoneId.of("UTC");
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm z").withZone(ZONE);

    public LocalDateTime currentDateTime() {
        return LocalDateTime.now(ZONE);
    }

    public String format(LocalDateTime localDateTime) {
        return FORMATTER.format(localDateTime);
    }

}
