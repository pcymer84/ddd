package com.fashionhouse.design.mapper;

import com.fashionhouse.design.dto.DesignRequest;
import com.fashionhouse.design.dto.DesignResponse;
import com.fashionhouse.design.model.Design;
import com.fashionhouse.design.model.DesignDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface DesignMapper {

    DesignDetails mapToDesignDetails(DesignRequest request);

    @Mappings({
            @Mapping(source = "design.name", target = "name"),
            @Mapping(source = "design.type", target = "type"),
            @Mapping(source = "design.description", target = "description"),
            @Mapping(source = "design.url", target = "url")
    })
    DesignResponse mapToDesignResponse(Design design);

}
