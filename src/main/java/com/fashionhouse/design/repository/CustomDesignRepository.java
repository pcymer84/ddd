package com.fashionhouse.design.repository;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.design.model.Design;
import java.util.UUID;

@Log
public interface CustomDesignRepository {

    Design getById(UUID designId);

    Design getByIdAndDesignerId(UUID designId, UUID designerId);

}
