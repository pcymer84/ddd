package com.fashionhouse.design.repository;

import com.fashionhouse.design.ex.DesignNotFoundException;
import com.fashionhouse.design.model.Design;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@AllArgsConstructor
@Repository
public class CustomDesignRepositoryImpl implements CustomDesignRepository {

    private final CrudDesignRepository designRepository;

    @Override
    public Design getById(UUID designId) {
        return designRepository.findById(designId)
                .orElseThrow(() -> new DesignNotFoundException(String.format("design with id '%s' not found", designId)));
    }

    @Override
    public Design getByIdAndDesignerId(UUID designId, UUID designerId) {
        return designRepository.findByIdAndDesignerId(designId, designerId)
                .orElseThrow(() -> new DesignNotFoundException(String.format("design with id '%s' for designer '%s' not found", designId, designerId)));
    }

}
