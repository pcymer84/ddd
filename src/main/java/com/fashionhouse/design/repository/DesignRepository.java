package com.fashionhouse.design.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface DesignRepository extends CrudDesignRepository, CustomDesignRepository {
}
