package com.fashionhouse.design.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.design.dto.DesignResponse;
import java.util.List;
import java.util.UUID;

public interface DesignQueryFacade {

    DesignResponse getDesign(User user, UUID designId);

    List<DesignResponse> getDesigns(User user);

}
