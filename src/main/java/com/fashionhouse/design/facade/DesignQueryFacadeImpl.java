package com.fashionhouse.design.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.design.dto.DesignResponse;
import com.fashionhouse.design.mapper.DesignMapper;
import com.fashionhouse.design.repository.DesignRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationService
@AllArgsConstructor
public class DesignQueryFacadeImpl implements DesignQueryFacade {

    private final DesignRepository designRepository;
    private final DesignMapper designMapper;

    @Override
    public DesignResponse getDesign(User user, UUID designId) {
        user.authorize(Permission.SHOW_DESIGN);
        return designMapper.mapToDesignResponse(designRepository.getById(designId));
    }

    @Override
    public List<DesignResponse> getDesigns(User user) {
        user.authorize(Permission.SHOW_DESIGN);
        return designRepository.findAll().stream()
                .map(designMapper::mapToDesignResponse)
                .collect(Collectors.toList());
    }

}
