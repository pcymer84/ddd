package com.fashionhouse.design.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.design.dto.DesignRequest;
import com.fashionhouse.design.dto.DesignResponse;
import java.util.UUID;

public interface DesignFacade {

    DesignResponse createDesign(User user, DesignRequest request);

    DesignResponse updateDesign(User user, UUID designId, DesignRequest request);

    DesignResponse finishDesign(User user, UUID designId);

    DesignResponse cancelDesign(User user, UUID designId);

}
