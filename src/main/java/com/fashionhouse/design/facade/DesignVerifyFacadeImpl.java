package com.fashionhouse.design.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.core.eventBus.EventPublisher;
import com.fashionhouse.design.dto.DesignResponse;
import com.fashionhouse.design.event.DesignEventFactory;
import com.fashionhouse.design.mapper.DesignMapper;
import com.fashionhouse.design.model.Design;
import com.fashionhouse.design.repository.DesignRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class DesignVerifyFacadeImpl implements DesignVerifyFacade {

    private final DesignRepository designRepository;
    private final DesignMapper designMapper;
    private final DesignEventFactory designEventFactory;
    private final EventPublisher eventPublisher;

    @Override
    public DesignResponse approveDesign(User user, UUID designId) {
        user.authorize(Permission.VERIFY_DESIGN);
        Design design = designRepository.getById(designId);
        design.approve(user.getId());
        designRepository.save(design);
        eventPublisher.publishEvent(designEventFactory.createDesignApprovedEvent(design));
        return designMapper.mapToDesignResponse(design);
    }

    @Override
    public DesignResponse declineDesign(User user, UUID designId) {
        user.authorize(Permission.VERIFY_DESIGN);
        Design design = designRepository.getById(designId);
        design.decline(user.getId());
        designRepository.save(design);
        return designMapper.mapToDesignResponse(design);
    }

}
