package com.fashionhouse.design.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.design.dto.DesignResponse;
import java.util.UUID;

public interface DesignVerifyFacade {

    DesignResponse approveDesign(User user, UUID designId);

    DesignResponse declineDesign(User user, UUID designId);

}
