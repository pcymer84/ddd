package com.fashionhouse.design.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.design.dto.DesignRequest;
import com.fashionhouse.design.dto.DesignResponse;
import com.fashionhouse.design.mapper.DesignMapper;
import com.fashionhouse.design.model.Design;
import com.fashionhouse.design.model.DesignFactory;
import com.fashionhouse.design.repository.DesignRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class DesignFacadeImpl implements DesignFacade {

    private final DesignRepository designRepository;
    private final DesignFactory designFactory;
    private final DesignMapper designMapper;

    @Override
    public DesignResponse createDesign(User user, DesignRequest request) {
        user.authorize(Permission.MAKE_DESIGN);
        Design design = designFactory.createDesign(user.getId(), request);
        designRepository.save(design);
        return designMapper.mapToDesignResponse(design);
    }

    @Override
    public DesignResponse updateDesign(User user, UUID designId, DesignRequest request) {
        user.authorize(Permission.MAKE_DESIGN);
        Design design = designRepository.getByIdAndDesignerId(designId, user.getId());
        design.changeDesign(designMapper.mapToDesignDetails(request));
        return designMapper.mapToDesignResponse(design);
    }

    @Override
    public DesignResponse finishDesign(User user, UUID designId) {
        user.authorize(Permission.MAKE_DESIGN);
        Design design = designRepository.getByIdAndDesignerId(designId, user.getId());
        design.finish();
        return designMapper.mapToDesignResponse(design);
    }

    @Override
    public DesignResponse cancelDesign(User user, UUID designId) {
        user.authorize(Permission.MAKE_DESIGN);
        Design design = designRepository.getById(designId);
        design.cancel();
        return designMapper.mapToDesignResponse(design);
    }

}
