package com.fashionhouse.design.ex;

import com.fashionhouse.core.ex.UnprocessableException;

public class InvalidDesignStatusException extends UnprocessableException {

    public InvalidDesignStatusException(String message) {
        super(message);
    }

}
