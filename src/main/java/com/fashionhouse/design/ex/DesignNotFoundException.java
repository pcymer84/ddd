package com.fashionhouse.design.ex;

import com.fashionhouse.core.ex.NotFoundException;

public class DesignNotFoundException extends NotFoundException {

    public DesignNotFoundException(String message) {
        super(message);
    }

}
