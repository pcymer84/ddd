package com.fashionhouse.design.model;

import com.fashionhouse.core.annotations.AggregateRoot;
import com.fashionhouse.core.util.DateTimeProvider;
import com.fashionhouse.core.validator.ValidationUtil;
import com.fashionhouse.design.ex.InvalidDesignStatusException;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@AggregateRoot
@Entity
@Builder
@Getter
@ToString
@Table(name="designs")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Design {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private DesignDetails design;
    @NotNull
    private UUID designerId;
    private UUID verifierId;
    @NotNull
    @Enumerated(EnumType.STRING)
    private DesignStatus status;
    @NotNull
    private LocalDateTime createdAt;
    private LocalDateTime verifiedAt;

    public void changeDesign(DesignDetails design) {
        validateModifiable();
        this.design = design;
    }

    public void cancel() {
        validateModifiable();
        status = DesignStatus.CANCELED;
    }

    public void finish() {
        validateModifiable();
        status = DesignStatus.FINISHED;
        verifiedAt = DateTimeProvider.currentDateTime();
    }

    public void approve(UUID approverId) {
        validateVerifiable();
        verifierId = approverId;
        status = DesignStatus.APPROVED;
        verifiedAt = DateTimeProvider.currentDateTime();
    }

    public void decline(UUID declineId) {
        validateVerifiable();
        verifierId = declineId;
        status = DesignStatus.DECLINED;
        verifiedAt = DateTimeProvider.currentDateTime();
    }

    private void validateModifiable() {
        if (!isModifiable()) {
            throw new InvalidDesignStatusException("design can not be modified");
        }
    }

    private void validateVerifiable() {
        if (!isVerifiable()) {
            throw new InvalidDesignStatusException("design can not be verified");
        }
    }

    private boolean isModifiable() {
        return status == DesignStatus.PROTOTYPED;
    }

    private boolean isVerifiable() {
        return status == DesignStatus.FINISHED;
    }

    private Design(Design.DesignBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        design = builder.design;
        designerId = builder.designerId;
        verifierId = builder.verifierId;
        status = builder.status;
        createdAt = builder.createdAt;
        verifiedAt = builder.verifiedAt;
        ValidationUtil.validate(this);
    }

    static class DesignBuilder {

        Design build() {
            return new Design(this);
        }

    }

}
