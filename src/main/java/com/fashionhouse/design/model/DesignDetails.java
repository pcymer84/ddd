package com.fashionhouse.design.model;

import com.fashionhouse.core.annotations.ValueObject;
import com.fashionhouse.core.validator.ValidationUtil;
import com.fashionhouse.core.validator.annotations.URL;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DesignDetails {

    @NotNull
    private String name;
    @NotNull
    private String type;
    @NotNull
    private String description;
    @URL
    @NotNull
    private String url;

    private DesignDetails(DesignDetails.DesignDetailsBuilder builder) {
        name = builder.name;
        type = builder.type;
        url = builder.url;
        description = builder.description;
        ValidationUtil.validate(this);
    }

    public static class DesignDetailsBuilder {

        public DesignDetails build() {
            return new DesignDetails(this);
        }

    }

}
