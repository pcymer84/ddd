package com.fashionhouse.design.model;

import com.fashionhouse.core.annotations.Factory;
import com.fashionhouse.core.util.DateTimeProvider;
import com.fashionhouse.design.dto.DesignRequest;
import com.fashionhouse.design.mapper.DesignMapper;
import lombok.AllArgsConstructor;
import java.util.UUID;

@Factory
@AllArgsConstructor
public class DesignFactory {

    private DesignMapper designMapper;

    public Design createDesign(UUID designerId, DesignRequest request) {
        return Design.builder()
                .designerId(designerId)
                .createdAt(DateTimeProvider.currentDateTime())
                .design(designMapper.mapToDesignDetails(request))
                .status(DesignStatus.PROTOTYPED)
                .build();
    }

}
