package com.fashionhouse.design.model;

public enum DesignStatus {

    CANCELED,
    PROTOTYPED,
    FINISHED,
    APPROVED,
    DECLINED

}
