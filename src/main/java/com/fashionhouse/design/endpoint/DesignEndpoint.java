package com.fashionhouse.design.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.design.dto.DesignRequest;
import com.fashionhouse.design.dto.DesignResponse;
import com.fashionhouse.design.facade.DesignFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class DesignEndpoint {

    private final DesignFacade designFacade;

    @PostMapping("/designs")
    public DesignResponse createDesign(@AuthenticationPrincipal User user,
                                       @Valid @RequestBody DesignRequest request) {
        return designFacade.createDesign(user, request);
    }

    @PutMapping("/designs/{designId}")
    public DesignResponse updateDesign(@AuthenticationPrincipal User user,
                                       @PathVariable("designId") UUID designId,
                                       @Valid @RequestBody DesignRequest request) {
        return designFacade.updateDesign(user, designId, request);
    }

    @PutMapping("/designs/{designId}/finish")
    public DesignResponse finishDesign(@AuthenticationPrincipal User user,
                                       @PathVariable("designId") UUID designId) {
        return designFacade.finishDesign(user, designId);
    }

    @PutMapping("/designs/{designId}/cancel")
    public DesignResponse cancelDesign(@AuthenticationPrincipal User user,
                                       @PathVariable("designId") UUID designId) {
        return designFacade.cancelDesign(user, designId);
    }

}
