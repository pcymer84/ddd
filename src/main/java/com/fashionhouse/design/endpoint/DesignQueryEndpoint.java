package com.fashionhouse.design.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.design.dto.DesignResponse;
import com.fashionhouse.design.facade.DesignQueryFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class DesignQueryEndpoint {

    private final DesignQueryFacade designQueryFacade;

    @GetMapping("/designs/{designId}")
    public DesignResponse getDesign(@AuthenticationPrincipal User user,
                                    @PathVariable("designId") UUID designId) {
        return designQueryFacade.getDesign(user, designId);
    }

    @GetMapping("/designs")
    public List<DesignResponse> getDesigns(@AuthenticationPrincipal User user) {
        return designQueryFacade.getDesigns(user);
    }

}
