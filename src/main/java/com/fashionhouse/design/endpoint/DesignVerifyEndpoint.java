package com.fashionhouse.design.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.design.dto.DesignResponse;
import com.fashionhouse.design.facade.DesignVerifyFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class DesignVerifyEndpoint {

    private final DesignVerifyFacade designVerifyFacade;

    @PutMapping("/designs/{designId}/approve")
    public DesignResponse approveDesign(@AuthenticationPrincipal User user,
                                        @PathVariable("designId") UUID designId) {
        return designVerifyFacade.approveDesign(user, designId);
    }

    @PutMapping("/designs/{designId}/decline")
    public DesignResponse declineDesign(@AuthenticationPrincipal User user,
                                        @PathVariable("designId") UUID designId) {
        return designVerifyFacade.declineDesign(user, designId);
    }

}
