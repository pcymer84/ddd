package com.fashionhouse.design.dto;

import com.fashionhouse.design.model.DesignStatus;
import lombok.Data;
import java.util.UUID;

@Data
public class DesignResponse {

    private UUID id;
    private String name;
    private String type;
    private String description;
    private String url;
    private DesignStatus status;

}
