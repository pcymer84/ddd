package com.fashionhouse.design.dto;

import com.fashionhouse.core.validator.annotations.URL;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class DesignRequest {

    @NotNull
    private String name;
    @NotNull
    private String type;
    @NotNull
    private String description;
    @URL
    @NotNull
    private String url;

}
