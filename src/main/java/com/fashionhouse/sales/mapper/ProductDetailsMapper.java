package com.fashionhouse.sales.mapper;

import com.fashionhouse.sales.model.Product;
import com.fashionhouse.sales.model.ProductDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface ProductDetailsMapper {

    @Mappings({@Mapping(source = "id", target = "productId")})
    ProductDetails mapToProductDetails(Product product);

}
