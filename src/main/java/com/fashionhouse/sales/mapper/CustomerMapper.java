package com.fashionhouse.sales.mapper;

import com.fashionhouse.sales.dto.CustomerAddressRequest;
import com.fashionhouse.sales.dto.CustomerRequest;
import com.fashionhouse.sales.dto.CustomerResponse;
import com.fashionhouse.sales.model.Address;
import com.fashionhouse.sales.model.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface CustomerMapper {

    CustomerResponse mapToCustomerResponse(Customer customer);

    void updateCustomer(@MappingTarget Customer customer, CustomerRequest customerRequest);

    Address mapToAddress(CustomerAddressRequest request);

}
