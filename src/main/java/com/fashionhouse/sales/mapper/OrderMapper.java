package com.fashionhouse.sales.mapper;

import com.fashionhouse.sales.dto.DeliveryRequest;
import com.fashionhouse.sales.dto.InvoiceRequest;
import com.fashionhouse.sales.dto.OrderEventResponse;
import com.fashionhouse.sales.dto.OrderProductResponse;
import com.fashionhouse.sales.dto.OrderResponse;
import com.fashionhouse.sales.dto.RecipientRequest;
import com.fashionhouse.sales.model.Delivery;
import com.fashionhouse.sales.model.Invoice;
import com.fashionhouse.sales.model.Order;
import com.fashionhouse.sales.model.OrderProduct;
import com.fashionhouse.sales.model.Recipient;
import com.fashionhouse.sales.model.event.OrderStatusEvent;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface OrderMapper {

    @Mappings({
            @Mapping(source = "orderProducts", target = "products"),
            @Mapping(source = "statusEvents", target = "events")
    })
    OrderResponse mapToOrderResponse(Order order);

    @Mappings({
            @Mapping(source = "product.productId", target = "productId"),
            @Mapping(source = "product.name", target = "name"),
            @Mapping(source = "product.price", target = "price"),
            @Mapping(source = "product.description", target = "description")
    })
    OrderProductResponse mapToOrderProductResponse(OrderProduct orderProduct);

    OrderEventResponse mapToOrderEventResponse(OrderStatusEvent orderEvent);

    Recipient mapToRecipient(RecipientRequest recipientRequest);

    Invoice mapToInvoice(InvoiceRequest invoiceRequest);

    Delivery mapToDelivery(DeliveryRequest deliveryRequest);

}
