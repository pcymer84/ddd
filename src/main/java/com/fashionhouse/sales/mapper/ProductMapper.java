package com.fashionhouse.sales.mapper;

import com.fashionhouse.sales.dto.ProductRequest;
import com.fashionhouse.sales.dto.ProductResponse;
import com.fashionhouse.sales.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface ProductMapper {

    ProductResponse mapToProductResponse(Product product);

    void updateProduct(@MappingTarget Product product, ProductRequest productRequest);

}
