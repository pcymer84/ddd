package com.fashionhouse.sales.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.DeliveryRequest;
import com.fashionhouse.sales.dto.InvoiceRequest;
import com.fashionhouse.sales.dto.OrderRequest;
import com.fashionhouse.sales.dto.OrderResponse;
import com.fashionhouse.sales.facade.OrderFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class OrderEndpoint {

    private final OrderFacade orderFacade;

    @PostMapping("/customers/me/orders")
    public OrderResponse submitOrder(@AuthenticationPrincipal User user,
                                     @Valid @RequestBody OrderRequest request) {
        return orderFacade.submitOrder(user, request);
    }

    @PutMapping("/customers/me/orders/{orderId}/finish")
    public OrderResponse finishOrder(@AuthenticationPrincipal User user,
                                     @PathVariable("orderId") UUID orderId) {
        return orderFacade.finishOrder(user, orderId);
    }

    @PutMapping("/customers/me/orders/{orderId}/refund")
    public OrderResponse refundOrder(@AuthenticationPrincipal User user,
                                     @PathVariable("orderId") UUID orderId) {
        return orderFacade.refundOrder(user, orderId);
    }

    @PutMapping("/customers/me/orders/{orderId}/delivery")
    public OrderResponse changeOrderDelivery(@AuthenticationPrincipal User user,
                                             @PathVariable("orderId") UUID orderId,
                                             @Valid @RequestBody DeliveryRequest request) {
        return orderFacade.changeOrderDelivery(user, orderId, request);
    }

    @PutMapping("/customers/me/orders/{orderId}/invoice")
    public OrderResponse changeOrderInvoice(@AuthenticationPrincipal User user,
                                            @PathVariable("orderId") UUID orderId,
                                            @Valid @RequestBody InvoiceRequest request) {
        return orderFacade.changeOrderInvoice(user, orderId, request);
    }

}
