package com.fashionhouse.sales.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.OrderResponse;
import com.fashionhouse.sales.facade.OrderFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class OrderSupportEndpoint {

    private final OrderFacade orderFacade;

    @PutMapping("/customers/{customerId}/orders/{orderId}/refund")
    public OrderResponse refundOrder(@AuthenticationPrincipal User user,
                                     @PathVariable("customerId") UUID customerId,
                                     @PathVariable("orderId") UUID orderId) {
        return orderFacade.refundOrder(user, customerId, orderId);
    }

}
