package com.fashionhouse.sales.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.OrderResponse;
import com.fashionhouse.sales.facade.OrderQueryFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class OrderQueryEndpoint {

    private final OrderQueryFacade orderFacade;

    @GetMapping("customers/me/orders/{orderId}")
    public OrderResponse getMyOrder(@AuthenticationPrincipal User user,
                                    @PathVariable("orderId") UUID orderId) {
        return orderFacade.getMyOrder(user, orderId);
    }

    @GetMapping("customers/me/orders")
    public List<OrderResponse> getMyOrders(@AuthenticationPrincipal User user) {
        return orderFacade.getMyOrders(user);
    }

    @GetMapping("/orders/{orderId}")
    public OrderResponse getOrder(@AuthenticationPrincipal User user,
                                  @PathVariable("orderId") UUID orderId) {
        return orderFacade.getOrder(user, orderId);
    }

    @GetMapping("/orders")
    public List<OrderResponse> getOrders(@AuthenticationPrincipal User user) {
        return orderFacade.getOrders(user);
    }

}
