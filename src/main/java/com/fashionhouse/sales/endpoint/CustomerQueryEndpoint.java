package com.fashionhouse.sales.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.CustomerResponse;
import com.fashionhouse.sales.facade.CustomerQueryFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class CustomerQueryEndpoint {

    private final CustomerQueryFacade customerQueryFacade;

    @GetMapping("/customers/me")
    public CustomerResponse getMyCustomer(@AuthenticationPrincipal User user) {
        return customerQueryFacade.getMyCustomer(user);
    }

    @GetMapping("/customers/{customerId}")
    public CustomerResponse getCustomer(@AuthenticationPrincipal User user,
                                        @PathVariable("customerId") UUID customerId) {
        return customerQueryFacade.getCustomer(user, customerId);
    }

    @GetMapping("/customers")
    public List<CustomerResponse> getCustomers(@AuthenticationPrincipal User user) {
        return customerQueryFacade.getCustomers(user);
    }

}
