package com.fashionhouse.sales.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.ProductRequest;
import com.fashionhouse.sales.dto.ProductResponse;
import com.fashionhouse.sales.facade.ProductManagementFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ProductManagementEndpoint {

    private final ProductManagementFacade productManagementFacade;

    @PutMapping("/products/{productId}")
    public ProductResponse changeProduct(@AuthenticationPrincipal User user,
                                         @PathVariable UUID productId,
                                         @Valid @RequestBody ProductRequest request) {
        return productManagementFacade.changeProduct(user, productId, request);
    }

    @PutMapping("/products/{productId}/available")
    public void availableProduct(@AuthenticationPrincipal User user,
                                 @PathVariable UUID productId) {
        productManagementFacade.availableProduct(user, productId);
    }

    @PutMapping("/products/{productId}/unavailable")
    public void unavailableProduct(@AuthenticationPrincipal User user,
                                   @PathVariable UUID productId) {
        productManagementFacade.unavailableProduct(user, productId);
    }

}
