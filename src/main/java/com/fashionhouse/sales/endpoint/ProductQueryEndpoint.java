package com.fashionhouse.sales.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.sales.dto.ProductResponse;
import com.fashionhouse.sales.facade.ProductQueryFacade;
import com.fashionhouse.sales.model.ProductStatus;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ProductQueryEndpoint {

    private final ProductQueryFacade productFacade;

    @GetMapping("/products")
    public List<ProductResponse> getProducts(@RequestParam(value = "status", required = false, defaultValue = "AVAILABLE") ProductStatus status) {
        return productFacade.getProducts(status);
    }

    @GetMapping("/products/{productId}")
    public ProductResponse getProduct(@PathVariable UUID productId) {
        return productFacade.getProduct(productId);
    }

}
