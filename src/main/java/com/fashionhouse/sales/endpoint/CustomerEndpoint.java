package com.fashionhouse.sales.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.CustomerAddressRequest;
import com.fashionhouse.sales.dto.CustomerRequest;
import com.fashionhouse.sales.dto.CustomerResponse;
import com.fashionhouse.sales.facade.CustomerFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class CustomerEndpoint {

    private final CustomerFacade customerFacade;

    @PutMapping("/customers/me")
    public CustomerResponse updateCustomer(@AuthenticationPrincipal User user,
                                           @Valid @RequestBody CustomerRequest request) {
        return customerFacade.updateCustomer(user, request);
    }

    @PostMapping("/customers/me/address")
    public CustomerResponse createAddress(@AuthenticationPrincipal User user,
                                          @Valid @RequestBody CustomerAddressRequest request) {
        return customerFacade.createAddress(user, request);
    }

    @DeleteMapping("/customers/me/address/{addressId}")
    public CustomerResponse removeAddress(@AuthenticationPrincipal User user,
                                          @PathVariable("addressId") UUID addressId) {
        return customerFacade.removeAddress(user, addressId);
    }


}
