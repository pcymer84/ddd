package com.fashionhouse.sales.ex;

import com.fashionhouse.core.ex.NotFoundException;

public class AddressNotFoundException extends NotFoundException {

    public AddressNotFoundException(String message) {
        super(message);
    }

}
