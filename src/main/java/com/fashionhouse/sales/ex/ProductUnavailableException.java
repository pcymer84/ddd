package com.fashionhouse.sales.ex;

import com.fashionhouse.core.ex.ClientSideException;

public class ProductUnavailableException extends ClientSideException {

    public ProductUnavailableException(String message) {
        super(message);
    }

}
