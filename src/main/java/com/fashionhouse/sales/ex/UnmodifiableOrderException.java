package com.fashionhouse.sales.ex;

import com.fashionhouse.core.ex.UnprocessableException;

public class UnmodifiableOrderException extends UnprocessableException {

    public UnmodifiableOrderException(String message) {
        super(message);
    }

}
