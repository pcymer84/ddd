package com.fashionhouse.sales.ex;

import com.fashionhouse.core.ex.UnprocessableException;

public class ProductOutOfStockException extends UnprocessableException {

    public ProductOutOfStockException(String message) {
        super(message);
    }

}
