package com.fashionhouse.sales.ex;

import com.fashionhouse.core.ex.ClientSideException;

public class ProductWithoutPriceException extends ClientSideException {

    public ProductWithoutPriceException(String message) {
        super(message);
    }

}
