package com.fashionhouse.sales.ex;

import com.fashionhouse.core.ex.NotFoundException;

public class ProductNotFoundException extends NotFoundException {

    public ProductNotFoundException(String message) {
        super(message);
    }

}
