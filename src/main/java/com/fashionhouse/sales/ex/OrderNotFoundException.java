package com.fashionhouse.sales.ex;

import com.fashionhouse.core.ex.NotFoundException;

public class OrderNotFoundException extends NotFoundException {

    public OrderNotFoundException(String message) {
        super(message);
    }

}
