package com.fashionhouse.sales.ex;

import com.fashionhouse.core.ex.UnprocessableException;

public class InvalidOrderStateException extends UnprocessableException {

    public InvalidOrderStateException(String message) {
        super(message);
    }

}
