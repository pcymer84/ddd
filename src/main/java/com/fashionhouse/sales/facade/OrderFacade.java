package com.fashionhouse.sales.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.DeliveryRequest;
import com.fashionhouse.sales.dto.InvoiceRequest;
import com.fashionhouse.sales.dto.OrderRequest;
import com.fashionhouse.sales.dto.OrderResponse;
import java.util.UUID;

public interface OrderFacade {

    OrderResponse submitOrder(User user, OrderRequest orderRequest);

    OrderResponse finishOrder(User user, UUID orderId);

    OrderResponse changeOrderDelivery(User user, UUID orderId, DeliveryRequest deliveryRequest);

    OrderResponse changeOrderInvoice(User user, UUID orderId, InvoiceRequest invoiceRequest);

    OrderResponse refundOrder(User user, UUID orderId);

    OrderResponse refundOrder(User user, UUID customerId, UUID orderId);

}
