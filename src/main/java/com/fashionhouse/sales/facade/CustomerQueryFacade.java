package com.fashionhouse.sales.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.CustomerResponse;
import java.util.List;
import java.util.UUID;

public interface CustomerQueryFacade {

    CustomerResponse getMyCustomer(User user);

    CustomerResponse getCustomer(User user, UUID customerId);

    List<CustomerResponse> getCustomers(User user);

}
