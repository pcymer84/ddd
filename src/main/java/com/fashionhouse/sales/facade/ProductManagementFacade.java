package com.fashionhouse.sales.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.ProductRequest;
import com.fashionhouse.sales.dto.ProductResponse;
import java.util.UUID;

public interface ProductManagementFacade {

    ProductResponse changeProduct(User user, UUID productId, ProductRequest request);

    void availableProduct(User user, UUID productId);

    void unavailableProduct(User user, UUID productId);

}
