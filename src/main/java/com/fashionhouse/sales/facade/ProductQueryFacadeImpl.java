package com.fashionhouse.sales.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.sales.dto.ProductResponse;
import com.fashionhouse.sales.mapper.ProductMapper;
import com.fashionhouse.sales.model.ProductStatus;
import com.fashionhouse.sales.repository.ProductRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationService
@AllArgsConstructor
public class ProductQueryFacadeImpl implements ProductQueryFacade {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Override
    public ProductResponse getProduct(UUID productId) {
        return productMapper.mapToProductResponse(productRepository.getById(productId));
    }

    @Override
    public List<ProductResponse> getProducts(ProductStatus status) {
        return productRepository.findByStatus(status).stream()
                .map(productMapper::mapToProductResponse)
                .collect(Collectors.toList());
    }

}
