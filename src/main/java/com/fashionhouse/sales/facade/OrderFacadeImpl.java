package com.fashionhouse.sales.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.core.eventBus.EventPublisher;
import com.fashionhouse.sales.dto.DeliveryRequest;
import com.fashionhouse.sales.dto.InvoiceRequest;
import com.fashionhouse.sales.dto.OrderRequest;
import com.fashionhouse.sales.dto.OrderResponse;
import com.fashionhouse.sales.event.OrderEventFactory;
import com.fashionhouse.sales.event.RefundOrderEvent;
import com.fashionhouse.sales.mapper.OrderMapper;
import com.fashionhouse.sales.model.Delivery;
import com.fashionhouse.sales.model.Invoice;
import com.fashionhouse.sales.model.Order;
import com.fashionhouse.sales.model.OrderFactory;
import com.fashionhouse.sales.repository.OrderRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class OrderFacadeImpl implements OrderFacade {

    private final OrderFactory orderFactory;
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;
    private final OrderEventFactory orderEventFactory;
    private final EventPublisher eventPublisher;

    @Override
    public OrderResponse submitOrder(User user, OrderRequest orderRequest) {
        Order order = orderFactory.createOrderToSubmit(user.getId(), orderRequest);
        orderRepository.save(order);
        eventPublisher.publishEvent(orderEventFactory.createSubmitOrderEvent(order));
        return orderMapper.mapToOrderResponse(order);
    }

    @Override
    public OrderResponse finishOrder(User user, UUID orderId) {
        Order order = orderRepository.getById(orderId);
        order.delivered();
        orderRepository.save(order);
        return orderMapper.mapToOrderResponse(order);
    }

    @Override
    public OrderResponse changeOrderDelivery(User user, UUID orderId, DeliveryRequest deliveryRequest) {
        Order order = orderRepository.getByIdAndBuyerId(orderId, user.getId());
        Delivery delivery = orderMapper.mapToDelivery(deliveryRequest);
        order.changeDelivery(delivery);
        orderRepository.save(order);
        eventPublisher.publishEvent(orderEventFactory.createOrderDeliveryChangeEvent(order));
        return orderMapper.mapToOrderResponse(order);
    }

    @Override
    public OrderResponse changeOrderInvoice(User user, UUID orderId, InvoiceRequest invoiceRequest) {
        Order order = orderRepository.getByIdAndBuyerId(orderId, user.getId());
        Invoice invoice = orderMapper.mapToInvoice(invoiceRequest);
        order.changeInvoice(invoice);
        orderRepository.save(order);
        eventPublisher.publishEvent(orderEventFactory.createOrderInvoiceChangeEvent(order));
        return orderMapper.mapToOrderResponse(order);
    }

    @Override
    public OrderResponse refundOrder(User user, UUID orderId) {
        Order order = orderRepository.getByIdAndBuyerId(orderId, user.getId());
        return refund(user, order);
    }

    @Override
    public OrderResponse refundOrder(User user, UUID customerId, UUID orderId) {
        user.authorize(Permission.ORDER_MANAGEMENT);
        Order order = orderRepository.getByIdAndBuyerId(orderId, customerId);
        return refund(user, order);
    }

    private OrderResponse refund(User user, Order order) {
        order.refund();
        orderRepository.save(order);
        eventPublisher.publishEvent(new RefundOrderEvent(order.getId(), user.getId()));
        return orderMapper.mapToOrderResponse(order);
    }

}
