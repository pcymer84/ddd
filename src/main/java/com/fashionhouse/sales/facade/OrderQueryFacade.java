package com.fashionhouse.sales.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.OrderResponse;
import java.util.List;
import java.util.UUID;

public interface OrderQueryFacade {

    OrderResponse getMyOrder(User user, UUID orderId);

    List<OrderResponse> getMyOrders(User user);

    OrderResponse getOrder(User user, UUID orderId);

    List<OrderResponse> getOrders(User user);

}
