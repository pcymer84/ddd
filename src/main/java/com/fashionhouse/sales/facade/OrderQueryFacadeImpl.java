package com.fashionhouse.sales.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.OrderResponse;
import com.fashionhouse.sales.mapper.OrderMapper;
import com.fashionhouse.sales.model.Order;
import com.fashionhouse.sales.repository.OrderRepository;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationService
@AllArgsConstructor
public class OrderQueryFacadeImpl implements OrderQueryFacade {

    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    @Override
    public OrderResponse getMyOrder(User user, UUID orderId) {
        Order order = orderRepository.getByIdAndBuyerId(orderId, user.getId());
        return orderMapper.mapToOrderResponse(order);
    }

    @Override
    public List<OrderResponse> getMyOrders(User user) {
        return orderRepository.findByBuyerId(user.getId()).stream()
                .map(orderMapper::mapToOrderResponse)
                .collect(Collectors.toList());
    }

    @Override
    public OrderResponse getOrder(User user, UUID orderId) {
        Order order = orderRepository.getById(orderId);
        return orderMapper.mapToOrderResponse(order);
    }

    @Override
    public List<OrderResponse> getOrders(User user) {
        user.authorize(Permission.SHOW_ORDERS);
        return orderRepository.findAll().stream()
                .map(orderMapper::mapToOrderResponse)
                .collect(Collectors.toList());
    }

}
