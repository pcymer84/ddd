package com.fashionhouse.sales.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.ProductRequest;
import com.fashionhouse.sales.dto.ProductResponse;
import com.fashionhouse.sales.mapper.ProductMapper;
import com.fashionhouse.sales.model.Product;
import com.fashionhouse.sales.repository.ProductRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class ProductManagementFacadeImpl implements ProductManagementFacade {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Override
    public ProductResponse changeProduct(User user, UUID productId, ProductRequest request) {
        user.authorize(Permission.PRODUCT_MANAGEMENT);
        Product product = productRepository.getById(productId);
        productMapper.updateProduct(product, request);
        productRepository.save(product);
        return productMapper.mapToProductResponse(product);
    }

    @Override
    public void availableProduct(User user, UUID productId) {
        user.authorize(Permission.PRODUCT_MANAGEMENT);
        Product product = productRepository.getById(productId);
        product.available();
        productRepository.save(product);
    }

    @Override
    public void unavailableProduct(User user, UUID productId) {
        user.authorize(Permission.PRODUCT_MANAGEMENT);
        Product product = productRepository.getById(productId);
        product.unavailable();
        productRepository.save(product);
    }

}
