package com.fashionhouse.sales.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.CustomerAddressRequest;
import com.fashionhouse.sales.dto.CustomerRequest;
import com.fashionhouse.sales.dto.CustomerResponse;
import com.fashionhouse.sales.mapper.CustomerMapper;
import com.fashionhouse.sales.model.Address;
import com.fashionhouse.sales.model.Customer;
import com.fashionhouse.sales.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class CustomerFacadeImpl implements CustomerFacade {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;

    @Override
    public CustomerResponse updateCustomer(User user, CustomerRequest request) {
        Customer customer = customerRepository.getById(user.getId());
        customerMapper.updateCustomer(customer, request);
        customerRepository.save(customer);
        return customerMapper.mapToCustomerResponse(customer);
    }

    @Override
    public CustomerResponse createAddress(User user, CustomerAddressRequest request) {
        Customer customer = customerRepository.getById(user.getId());
        Address address = customerMapper.mapToAddress(request);
        customer.addAddress(address);
        customerRepository.save(customer);
        return customerMapper.mapToCustomerResponse(customer);
    }

    @Override
    public CustomerResponse removeAddress(User user, UUID addressId) {
        Customer customer = customerRepository.getById(user.getId());
        customer.removeAddress(addressId);
        customerRepository.save(customer);
        return customerMapper.mapToCustomerResponse(customer);
    }

}
