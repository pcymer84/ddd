package com.fashionhouse.sales.facade;

import com.fashionhouse.sales.dto.ProductResponse;
import com.fashionhouse.sales.model.ProductStatus;
import java.util.List;
import java.util.UUID;

public interface ProductQueryFacade {

    ProductResponse getProduct(UUID productId);

    List<ProductResponse> getProducts(ProductStatus status);

}
