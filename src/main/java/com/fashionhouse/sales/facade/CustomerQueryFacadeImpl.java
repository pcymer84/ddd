package com.fashionhouse.sales.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.CustomerResponse;
import com.fashionhouse.sales.mapper.CustomerMapper;
import com.fashionhouse.sales.repository.CustomerRepository;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationService
@AllArgsConstructor
public class CustomerQueryFacadeImpl implements CustomerQueryFacade {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;

    @Override
    public CustomerResponse getMyCustomer(User user) {
        return customerMapper.mapToCustomerResponse(customerRepository.getById(user.getId()));
    }

    @Override
    public CustomerResponse getCustomer(User user, UUID customerId) {
        user.authorize(Permission.SHOW_CUSTOMERS);
        return customerMapper.mapToCustomerResponse(customerRepository.getById(customerId));
    }

    @Override
    public List<CustomerResponse> getCustomers(User user) {
        user.authorize(Permission.SHOW_CUSTOMERS);
        return customerRepository.findAll().stream()
                .map(customerMapper::mapToCustomerResponse)
                .collect(Collectors.toList());
    }

}
