package com.fashionhouse.sales.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.sales.dto.CustomerAddressRequest;
import com.fashionhouse.sales.dto.CustomerRequest;
import com.fashionhouse.sales.dto.CustomerResponse;
import java.util.UUID;

public interface CustomerFacade {

    CustomerResponse updateCustomer(User user, CustomerRequest request);

    CustomerResponse createAddress(User user, CustomerAddressRequest request);

    CustomerResponse removeAddress(User user, UUID addressId);

}
