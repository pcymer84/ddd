package com.fashionhouse.sales.event;

import com.fashionhouse.core.eventBus.EventSubscriber;
import com.fashionhouse.core.logger.Log;
import com.fashionhouse.sales.model.Order;
import com.fashionhouse.sales.repository.OrderRepository;
import com.fashionhouse.warehouse.event.ParcelPackedEvent;
import com.fashionhouse.warehouse.event.ParcelReturnedEvent;
import com.fashionhouse.warehouse.event.ParcelSentEvent;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;

@Log
@EventSubscriber
@AllArgsConstructor
public class OrderEventSubscriber {

    private final OrderRepository orderRepository;

    @Subscribe
    public void onParcelPackedEvent(ParcelPackedEvent event) {
        Order order = orderRepository.getById(event.getOrderId());
        order.packed();
        orderRepository.save(order);
    }

    @Subscribe
    public void onParcelSentEvent(ParcelSentEvent event) {
        Order order = orderRepository.getById(event.getOrderId());
        order.sent();
        orderRepository.save(order);
    }

    @Subscribe
    public void onParcelReturnedEvent(ParcelReturnedEvent event) {
        Order order = orderRepository.getById(event.getOrderId());
        order.returned();
        orderRepository.save(order);
    }

}
