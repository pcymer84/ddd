package com.fashionhouse.sales.event;

import com.fashionhouse.account.event.AccountCreatedEvent;
import com.fashionhouse.account.event.AccountEmailChangedEvent;
import com.fashionhouse.core.eventBus.EventSubscriber;
import com.fashionhouse.core.logger.Log;
import com.fashionhouse.sales.model.Customer;
import com.fashionhouse.sales.repository.CustomerRepository;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;

@Log
@EventSubscriber
@AllArgsConstructor
public class CustomerEventSubscriber {

    private final CustomerRepository customerRepository;

    @Subscribe
    public void onAccountCreatedEvent(AccountCreatedEvent event) {
        Customer customer = createCustomer(event);
        customerRepository.save(customer);
    }

    @Subscribe
    public void onAccountEmailChangedEvent(AccountEmailChangedEvent event) {
        Customer customer = customerRepository.getById(event.getAccountId());
        customer.setEmail(event.getEmail());
        customerRepository.save(customer);
    }

    private Customer createCustomer(AccountCreatedEvent event) {
        return Customer.builder()
                .id(event.getAccountId())
                .email(event.getEmail())
                .build();
    }

}
