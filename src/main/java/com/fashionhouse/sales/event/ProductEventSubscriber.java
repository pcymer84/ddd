package com.fashionhouse.sales.event;

import com.fashionhouse.core.eventBus.EventSubscriber;
import com.fashionhouse.core.logger.Log;
import com.fashionhouse.design.event.DesignApprovedEvent;
import com.fashionhouse.sales.model.Product;
import com.fashionhouse.sales.model.ProductStatus;
import com.fashionhouse.sales.repository.ProductRepository;
import com.fashionhouse.warehouse.event.ItemSupplyEvent;
import com.fashionhouse.warehouse.event.ItemDrawEvent;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;
import java.util.Map;
import java.util.UUID;

@Log
@EventSubscriber
@AllArgsConstructor
public class ProductEventSubscriber {

    private final ProductRepository productRepository;

    @Subscribe
    public void onItemSupplyEvent(ItemSupplyEvent event) {
        Map<UUID, Product> products = productRepository.getProductsById(event.getItemIds());
        event.getSupplies().forEach(supply -> products.get(supply.getItemId()).setStock(supply.getStock()));
        productRepository.saveAll(products);
    }

    @Subscribe
    public void onItemDrawEvent(ItemDrawEvent event) {
        Map<UUID, Product> products = productRepository.getProductsById(event.getItemIds());
        event.getDraws().forEach(draw -> products.get(draw.getItemId()).setStock(draw.getStock()));
        productRepository.saveAll(products);
    }

    @Subscribe
    public void onDesignApprovedEvent(DesignApprovedEvent event) {
        productRepository.save(createProduct(event));
    }

    private Product createProduct(DesignApprovedEvent event) {
        return Product.builder()
                .id(event.getDesignId())
                .stock(0)
                .status(ProductStatus.UNAVAILABLE)
                .name(event.getName())
                .description(event.getDescription())
                .build();
    }

}
