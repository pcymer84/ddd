package com.fashionhouse.sales.event;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import java.time.LocalDateTime;
import java.util.UUID;

@ToString
@Value
@Builder
public class OrderInvoiceChangeEvent {

    private UUID orderId;
    private String name;
    private String nip;
    private String address;
    private LocalDateTime date;

}
