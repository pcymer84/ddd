package com.fashionhouse.sales.event;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.sales.model.Order;
import com.fashionhouse.sales.model.OrderProduct;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Log
@Component
public class OrderEventFactory {

    public SubmitOrderEvent createSubmitOrderEvent(Order order) {
        return SubmitOrderEvent.builder()
                .orderId(order.getId())
                .buyerId(order.getBuyerId())
                .delivery(order.getDelivery())
                .invoice(order.getInvoice())
                .recipient(order.getRecipient())
                .orderProducts(createFromOrderProducts(order))
                .build();
    }

    public OrderDeliveryChangeEvent createOrderDeliveryChangeEvent(Order order) {
        return OrderDeliveryChangeEvent.builder()
                .orderId(order.getId())
                .postcode(order.getDelivery().getPostcode())
                .city(order.getDelivery().getCity())
                .street(order.getDelivery().getStreet())
                .description(order.getDelivery().getDescription())
                .build();
    }

    public OrderInvoiceChangeEvent createOrderInvoiceChangeEvent(Order order) {
        return OrderInvoiceChangeEvent.builder()
                .orderId(order.getId())
                .name(order.getInvoice().getName())
                .nip(order.getInvoice().getNip())
                .address(order.getInvoice().getAddress())
                .build();
    }

    private List<OrderProductDetails> createFromOrderProducts(Order order) {
        return order.getOrderProducts().stream()
                .map(this::createFromOrderProduct)
                .collect(Collectors.toList());
    }

    private OrderProductDetails createFromOrderProduct(OrderProduct orderProduct) {
        return new OrderProductDetails(
                orderProduct.getProduct().getProductId(),
                orderProduct.getAmount());
    }

}
