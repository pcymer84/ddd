package com.fashionhouse.sales.event;

import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.Value;

import java.util.UUID;

@ToString
@Value
@AllArgsConstructor
public class OrderProductDetails {

    private UUID productId;
    private Integer amount;

    public boolean isProductId(UUID productId) {
        return this.productId.equals(productId);
    }

}
