package com.fashionhouse.sales.repository;

import com.fashionhouse.sales.ex.ProductNotFoundException;
import com.fashionhouse.sales.model.Product;
import com.fashionhouse.warehouse.model.Item;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
@Repository
public class CustomProductRepositoryImpl implements CustomProductRepository {

    private final CrudProductRepository productRepository;

    @Override
    public Product getById(UUID productId) {
        return productRepository.findById(productId).orElseThrow(() -> new ProductNotFoundException(String.format("product with id '%s' not found", productId)));
    }

    @Override
    public Map<UUID, Product> getProductsById(Iterable<UUID> ids) {
        return productRepository.findAllById(ids).stream().collect(Collectors.toMap(Product::getId, product -> product));
    }

    @Override
    public Iterable<Product> saveAll(Map<UUID, Product> products) {
        return productRepository.saveAll(products.values());
    }

}
