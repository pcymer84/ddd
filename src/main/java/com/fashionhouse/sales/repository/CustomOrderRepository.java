package com.fashionhouse.sales.repository;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.sales.model.Order;
import java.util.UUID;

@Log
public interface CustomOrderRepository {

    Order getById(UUID orderId);

    Order getByIdAndBuyerId(UUID orderId, UUID buyerId);

}
