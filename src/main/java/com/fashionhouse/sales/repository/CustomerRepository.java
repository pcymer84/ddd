package com.fashionhouse.sales.repository;

import com.fashionhouse.core.logger.Log;
import org.springframework.stereotype.Repository;

@Log
@Repository
public interface CustomerRepository extends CrudCustomerRepository, CustomCustomerRepository {
}
