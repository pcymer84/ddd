package com.fashionhouse.sales.repository;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.sales.model.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

@Log
public interface CrudCustomerRepository extends CrudRepository<Customer, UUID> {

    List<Customer> findAll();

}
