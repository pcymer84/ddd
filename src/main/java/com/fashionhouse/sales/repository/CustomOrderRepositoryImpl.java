package com.fashionhouse.sales.repository;

import com.fashionhouse.sales.ex.OrderNotFoundException;
import com.fashionhouse.sales.model.Order;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
@AllArgsConstructor
public class CustomOrderRepositoryImpl implements CustomOrderRepository {

    private final CrudOrderRepository orderRepository;

    @Override
    public Order getById(UUID orderId) {
        return orderRepository.findById(orderId).orElseThrow(() -> new OrderNotFoundException(String.format("order with id '%s' not found", orderId)));
    }

    @Override
    public Order getByIdAndBuyerId(UUID orderId, UUID buyerId) {
        return orderRepository.findByIdAndBuyerId(orderId, buyerId).orElseThrow(() -> new OrderNotFoundException(String.format("order with id '%s' for buyer '%s' not found", orderId, buyerId)));
    }

}
