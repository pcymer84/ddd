package com.fashionhouse.sales.repository;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.sales.model.Customer;
import java.util.UUID;

@Log
public interface CustomCustomerRepository {

    Customer getById(UUID customerId);

}
