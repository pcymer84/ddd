package com.fashionhouse.sales.repository;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.sales.model.Product;

import java.util.Map;
import java.util.UUID;

@Log
public interface CustomProductRepository {

    Product getById(UUID productId);

    Map<UUID, Product> getProductsById(Iterable<UUID> ids);

    Iterable<Product> saveAll(Map<UUID, Product> products);

}
