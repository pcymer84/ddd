package com.fashionhouse.sales.dto;

import com.fashionhouse.core.validator.annotations.Currency;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class ProductRequest {

    @NotNull
    @Currency
    private Double price;
    @NotNull
    private String name;
    private String description;

}
