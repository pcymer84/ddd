package com.fashionhouse.sales.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceResponse {

    private String name;
    private String nip;
    private String address;
    private LocalDateTime date;

}
