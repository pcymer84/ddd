package com.fashionhouse.sales.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class ProductResponse {

    private UUID id;
    private Double price;
    private String name;
    private String description;
    private Integer stock;

}
