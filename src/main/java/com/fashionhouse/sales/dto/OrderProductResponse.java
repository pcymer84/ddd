package com.fashionhouse.sales.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderProductResponse {

    private UUID productId;
    private Integer amount;
    private String name;
    private Double price;
    private String description;

}
