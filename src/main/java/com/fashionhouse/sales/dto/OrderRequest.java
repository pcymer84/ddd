package com.fashionhouse.sales.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequest {

    @NotNull
    @NotEmpty
    private List<OrderProductRequest> products;
    @NotNull
    private RecipientRequest recipient;
    @NotNull
    private DeliveryRequest delivery;
    private InvoiceRequest invoice;

}
