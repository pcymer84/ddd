package com.fashionhouse.sales.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class CustomerAddressResponse {

    private UUID id;
    private String postcode;
    private String city;
    private String street;
    private String description;

}
