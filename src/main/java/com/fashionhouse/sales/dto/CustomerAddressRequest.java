package com.fashionhouse.sales.dto;

import lombok.Data;

@Data
public class CustomerAddressRequest {

    private String postcode;
    private String city;
    private String street;
    private String description;

}
