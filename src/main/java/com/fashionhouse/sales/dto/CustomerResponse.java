package com.fashionhouse.sales.dto;

import lombok.Data;
import java.util.List;
import java.util.UUID;

@Data
public class CustomerResponse {

    private UUID id;
    private String email;
    private String phone;
    private String firstName;
    private String lastName;
    private List<CustomerAddressResponse> addresses;

}
