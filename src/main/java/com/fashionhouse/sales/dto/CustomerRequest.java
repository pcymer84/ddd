package com.fashionhouse.sales.dto;

import lombok.Data;

@Data
public class CustomerRequest {

    private String email;
    private String phone;
    private String firstName;
    private String lastName;

}
