package com.fashionhouse.sales.model;

import com.fashionhouse.core.annotations.ValueObject;
import com.fashionhouse.core.validator.ValidationUtil;
import com.fashionhouse.core.validator.annotations.Currency;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductDetails {

    @NotNull
    @Column(name = "product_id")
    private UUID productId;
    @NotNull
    private String name;
    @NotNull
    @Currency
    private Double price;
    private String description;

    private ProductDetails(ProductDetails.ProductDetailsBuilder builder) {
        productId = builder.productId;
        name = builder.name;
        price = builder.price;
        description = builder.description;
        ValidationUtil.validate(this);
    }

    public static class ProductDetailsBuilder {

        public ProductDetails build() {
            return new ProductDetails(this);
        }

    }

}
