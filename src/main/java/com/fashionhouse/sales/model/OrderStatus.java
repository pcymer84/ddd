package com.fashionhouse.sales.model;

public enum OrderStatus {

    REFUND,
    SUBMITTED,
    PACKED,
    SENT,
    RETURNED,
    DELIVERED

}