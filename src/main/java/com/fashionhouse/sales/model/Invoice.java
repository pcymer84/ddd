package com.fashionhouse.sales.model;

import com.fashionhouse.core.annotations.ValueObject;
import com.fashionhouse.core.validator.ValidationUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Invoice {

    @NotNull
    private String name;
    private String nip;
    private String address;
    private LocalDateTime date;

    private Invoice(Invoice.InvoiceBuilder builder) {
        name = builder.name;
        nip = builder.nip;
        address = builder.address;
        date = builder.date;
        ValidationUtil.validate(this);
    }

    public static class InvoiceBuilder {

        public Invoice build() {
            return new Invoice(this);
        }

    }

}
