package com.fashionhouse.sales.model;

import com.fashionhouse.core.annotations.AggregateRoot;
import com.fashionhouse.core.validator.ValidationUtil;
import com.fashionhouse.sales.ex.InvalidOrderStateException;
import com.fashionhouse.sales.ex.UnmodifiableOrderException;
import com.fashionhouse.sales.model.event.OrderEvent;
import com.fashionhouse.sales.model.event.OrderStatusEvent;
import com.google.common.collect.Sets;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@AggregateRoot
@Entity
@Getter
@Builder
@ToString
@Table(name="orders")
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Order {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private UUID buyerId;
    @NotNull
    @Embedded
    private Recipient recipient;
    @Embedded
    private Invoice invoice;
    @NotNull
    @Embedded
    private Delivery delivery;
    @NotEmpty
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "order_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<OrderProduct> orderProducts;
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "order_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<OrderEvent> events;

    public List<OrderStatusEvent> getStatusEvents() {
        return events.stream()
                .filter(this::isStatusEvent)
                .sorted(OrderEvent::sortNewest)
                .map(event -> ((OrderStatusEvent) event))
                .collect(Collectors.toList());
    }

    private boolean isStatusEvent(OrderEvent event) {
        return event instanceof OrderStatusEvent;
    }

    public Set<OrderProduct> getOrderProducts() {
        return Sets.newHashSet(orderProducts);
    }

    public void changeInvoice(Invoice invoice) {
        validateOrderModifiable();
        this.invoice = invoice;
        ValidationUtil.validate(this);
    }

    public void changeDelivery(Delivery delivery) {
        validateOrderModifiable();
        this.delivery = delivery;
        ValidationUtil.validate(this);
    }

    public void packed() {
        validateOrderNotFinished();
        events.add(new OrderStatusEvent(OrderStatus.PACKED));
    }

    public void returned() {
        validateOrderNotFinished();
        events.add(new OrderStatusEvent(OrderStatus.RETURNED));
    }

    public void sent() {
        validateOrderNotFinished();
        events.add(new OrderStatusEvent(OrderStatus.SENT));
    }

    public void delivered() {
        validateOrderNotFinished();
        events.add(new OrderStatusEvent(OrderStatus.DELIVERED));
    }

    public void refund() {
        validateOrderNotRefunded();
        events.add(new OrderStatusEvent(OrderStatus.REFUND));
    }

    private void validateOrderModifiable() {
        if (!isOrderModifiable()) {
            throw new UnmodifiableOrderException(String.format("order '%s' is unmodifiable", id));
        }
    }

    private void validateOrderNotFinished() {
        if (isFinished()) {
            throw new InvalidOrderStateException(String.format("order '%s' is finished", id));
        }
    }

    private void validateOrderNotRefunded() {
        if (isRefunded()) {
            throw new InvalidOrderStateException(String.format("order '%s' is refunded", id));
        }
    }

    private boolean isOrderModifiable() {
        OrderStatus currentStatus = getCurrentStatus();
        return currentStatus == OrderStatus.SUBMITTED || currentStatus == OrderStatus.PACKED;
    }

    private boolean isFinished() {
        OrderStatus currentStatus = getCurrentStatus();
        return currentStatus == OrderStatus.REFUND || currentStatus == OrderStatus.DELIVERED;
    }

    private boolean isRefunded() {
        return getStatusEvents().stream().anyMatch(statusEvent -> statusEvent.hasStatus(OrderStatus.REFUND));
    }

    private OrderStatus getCurrentStatus() {
        return getStatusEvents().stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("order '%s' without status", id)))
                .getStatus();
    }

    private Order(Order.OrderBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        buyerId = builder.buyerId;
        recipient = builder.recipient;
        delivery = builder.delivery;
        invoice = builder.invoice;
        orderProducts = CollectionUtils.isEmpty(builder.orderProducts) ? Sets.newHashSet() : Sets.newHashSet(builder.orderProducts);
        events = CollectionUtils.isEmpty(builder.events) ? Sets.newHashSet() : Sets.newHashSet(builder.events);
        ValidationUtil.validate(this);
    }

    static class OrderBuilder {

        Order build() {
            return new Order(this);
        }

    }

}
