package com.fashionhouse.sales.model;

import com.fashionhouse.core.annotations.AggregateRoot;
import com.fashionhouse.core.validator.ValidationUtil;
import com.fashionhouse.core.validator.annotations.Currency;
import com.fashionhouse.sales.ex.ProductOutOfStockException;
import com.fashionhouse.sales.ex.ProductUnavailableException;
import com.fashionhouse.sales.ex.ProductWithoutPriceException;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@AggregateRoot
@Entity
@Getter
@Builder
@ToString
@Table(name="products")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Product {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    @Setter
    private String name;
    @Currency
    @Setter
    private Double price;
    @NotNull
    @Setter
    private String description;
    @NotNull
    @Setter
    private Integer stock;
    @NotNull
    @Enumerated(EnumType.STRING)
    private ProductStatus status;

    public void validateSaleability(Integer amount) {
        validateAvailability();
        validatePrice();
        validateStock(amount);
    }

    public void available() {
        validatePrice();
        status = ProductStatus.AVAILABLE;
    }

    public void unavailable() {
        status = ProductStatus.UNAVAILABLE;
    }

    private void validateStock(Integer quantity) {
        if (stock < quantity) {
            throw new ProductOutOfStockException(String.format("not enough product '%s' in stock", id));
        }
    }

    private void validateAvailability() {
        if (status == ProductStatus.UNAVAILABLE) {
            throw new ProductUnavailableException(String.format("product '%s' is unavailable", id));
        }
    }

    private void validatePrice() {
        if (price == null) {
            throw new ProductWithoutPriceException(String.format("product '%s' without price", id));
        }
    }

    private Product(Product.ProductBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        name = builder.name;
        price = builder.price;
        description = builder.description;
        stock = builder.stock;
        status = builder.status;
        ValidationUtil.validate(this);
    }

    public static class ProductBuilder {

        public Product build() {
            return new Product(this);
        }

    }

}
