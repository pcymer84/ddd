package com.fashionhouse.sales.model;

import com.fashionhouse.core.annotations.Factory;
import com.fashionhouse.sales.dto.OrderProductRequest;
import com.fashionhouse.sales.mapper.ProductDetailsMapper;
import com.fashionhouse.sales.repository.ProductRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Factory
@AllArgsConstructor
public class OrderProductFactory {

    private final ProductRepository productRepository;
    private final ProductDetailsMapper productDetailsMapper;

    public Set<OrderProduct> createOrderProducts(List<OrderProductRequest> orderProducts) {
        return orderProducts.stream().map(this::createOrderProduct).collect(Collectors.toSet());
    }

    public OrderProduct createOrderProduct(OrderProductRequest orderProductRequest) {
        return createOrderProduct(orderProductRequest.getProductId(), orderProductRequest.getAmount());
    }

    public OrderProduct createOrderProduct(UUID productId, Integer amount) {
        Product product = productRepository.getById(productId);
        product.validateSaleability(amount);
        return OrderProduct.builder()
                .product(productDetailsMapper.mapToProductDetails(product))
                .amount(amount)
                .build();
    }

}
