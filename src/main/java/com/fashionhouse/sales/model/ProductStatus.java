package com.fashionhouse.sales.model;

public enum ProductStatus {

    AVAILABLE,
    UNAVAILABLE

}
