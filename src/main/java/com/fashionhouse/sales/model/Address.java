package com.fashionhouse.sales.model;

import com.fashionhouse.core.validator.ValidationUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@ToString
@Getter
@Builder
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Table(name="customer_addresses")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Address {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private String postcode;
    @NotNull
    private String city;
    @NotNull
    private String street;
    private String description;

    private Address(Address.AddressBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        postcode = builder.postcode;
        city = builder.city;
        street = builder.street;
        description = builder.description;
        ValidationUtil.validate(this);
    }

    public static class AddressBuilder {

        public Address build() {
            return new Address(this);
        }

    }

}
