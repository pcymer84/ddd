package com.fashionhouse.sales.model;

import com.fashionhouse.core.annotations.ValueObject;
import com.fashionhouse.core.validator.ValidationUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Recipient {

    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    private String email;
    private String phone;

    private Recipient(Recipient.RecipientBuilder builder) {
        firstName = builder.firstName;
        lastName = builder.lastName;
        email = builder.email;
        phone = builder.phone;
        ValidationUtil.validate(this);
    }

    public static class RecipientBuilder {

        public Recipient build() {
            return new Recipient(this);
        }

    }

}
