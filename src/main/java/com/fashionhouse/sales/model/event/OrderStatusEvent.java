package com.fashionhouse.sales.model.event;

import com.fashionhouse.sales.model.OrderStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@ToString(callSuper = true)
@Entity
@Getter
@NoArgsConstructor
@DiscriminatorValue("OrderStatus")
public class OrderStatusEvent extends OrderEvent {

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    public OrderStatusEvent(OrderStatus status) {
        this();
        this.status = status;
    }

    public boolean hasStatus(OrderStatus status) {
        return this.status == status;
    }

}
