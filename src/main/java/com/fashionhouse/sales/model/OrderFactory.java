package com.fashionhouse.sales.model;

import com.fashionhouse.core.annotations.Factory;
import com.fashionhouse.sales.dto.OrderRequest;
import com.fashionhouse.sales.mapper.OrderMapper;
import com.fashionhouse.sales.model.event.OrderStatusEvent;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import java.util.Set;
import java.util.UUID;

@Factory
@AllArgsConstructor
public class OrderFactory {

    private final OrderProductFactory orderProductFactory;
    private final OrderMapper orderMapper;

    public Order createOrderToSubmit(UUID buyerId, OrderRequest request) {
        Set<OrderProduct> orderProducts = orderProductFactory.createOrderProducts(request.getProducts());
        return Order.builder()
                .buyerId(buyerId)
                .orderProducts(orderProducts)
                .recipient(orderMapper.mapToRecipient(request.getRecipient()))
                .delivery(orderMapper.mapToDelivery(request.getDelivery()))
                .invoice(orderMapper.mapToInvoice(request.getInvoice()))
                .events(Sets.newHashSet(new OrderStatusEvent(OrderStatus.SUBMITTED)))
                .build();
    }

}
