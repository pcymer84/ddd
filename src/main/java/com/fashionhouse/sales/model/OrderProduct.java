package com.fashionhouse.sales.model;

import com.fashionhouse.core.validator.ValidationUtil;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Getter
@Builder
@ToString
@EqualsAndHashCode(of = "id")
@Table(name="order_products")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderProduct {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    @Min(1)
    private Integer amount;
    @NotNull
    @Embedded
    private ProductDetails product;

    private OrderProduct(OrderProduct.OrderProductBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        amount = builder.amount;
        product = builder.product;
        ValidationUtil.validate(this);
    }

    public static class OrderProductBuilder {

        public OrderProduct build() {
            return new OrderProduct(this);
        }

    }

}
