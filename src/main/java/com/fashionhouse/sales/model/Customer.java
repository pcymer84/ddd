package com.fashionhouse.sales.model;

import com.fashionhouse.core.annotations.AggregateRoot;
import com.fashionhouse.core.validator.ValidationUtil;
import com.fashionhouse.sales.ex.AddressNotFoundException;
import com.google.common.collect.Sets;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@AggregateRoot
@Entity
@Builder
@Getter
@ToString
@EqualsAndHashCode(of = "id")
@Table(name="customers")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Customer {

    @Id
    @NotNull
    private UUID id;
    @Setter
    private String email;
    @Setter
    private String phone;
    @Setter
    private String firstName;
    @Setter
    private String lastName;
    @NotEmpty
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "customer_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Address> addresses;

    public Set<Address> getAddresses() {
        return Sets.newHashSet(addresses);
    }

    public void addAddress(Address address) {
        addresses.add(address);
    }

    public void removeAddress(UUID addressId) {
        addresses.remove(findAddress(addressId));
    }

    private Address findAddress(UUID addressId) {
        return addresses.stream()
                .filter(address -> address.getId().equals(addressId))
                .findAny()
                .orElseThrow(() -> new AddressNotFoundException(String.format("address '%s' not found for customer '%s'", addressId, id)));
    }

    private Customer(Customer.CustomerBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        email = builder.email;
        phone = builder.phone;
        firstName = builder.firstName;
        lastName = builder.lastName;
        addresses = CollectionUtils.isEmpty(builder.addresses) ? Sets.newHashSet() :  builder.addresses;
        ValidationUtil.validate(this);
    }

    public static class CustomerBuilder {

        public Customer build() {
            return new Customer(this);
        }

    }

}
