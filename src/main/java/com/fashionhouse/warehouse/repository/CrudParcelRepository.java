package com.fashionhouse.warehouse.repository;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.warehouse.model.Parcel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log
public interface CrudParcelRepository extends CrudRepository<Parcel, UUID> {

    Optional<Parcel> findByOrderId(UUID orderId);

    List<Parcel> findAll();

}
