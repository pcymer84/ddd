package com.fashionhouse.warehouse.repository;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.warehouse.model.Item;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.UUID;

@Log
public interface CrudItemRepository extends CrudRepository<Item, UUID> {

    @Override
    List<Item> findAllById(Iterable<UUID> ids);

    @Override
    List<Item> findAll();

}
