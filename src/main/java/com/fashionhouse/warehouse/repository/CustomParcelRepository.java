package com.fashionhouse.warehouse.repository;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.warehouse.model.Parcel;
import java.util.UUID;

@Log
public interface CustomParcelRepository {

    Parcel getById(UUID parcelId);

    Parcel getByOrderId(UUID orderId);

}
