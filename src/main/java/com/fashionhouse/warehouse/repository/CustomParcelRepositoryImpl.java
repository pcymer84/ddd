package com.fashionhouse.warehouse.repository;

import com.fashionhouse.warehouse.ex.ParcelNotFoundException;
import com.fashionhouse.warehouse.model.Parcel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
@AllArgsConstructor
public class CustomParcelRepositoryImpl implements CustomParcelRepository {

    private final CrudParcelRepository parcelRepository;

    @Override
    public Parcel getById(UUID parcelId) {
        return parcelRepository.findById(parcelId).orElseThrow(() -> new ParcelNotFoundException(String.format("parcel with id '%s' not found", parcelId)));
    }

    @Override
    public Parcel getByOrderId(UUID orderId) {
        return parcelRepository.findByOrderId(orderId).orElseThrow(() -> new ParcelNotFoundException(String.format("parcel with order id '%s' not found", orderId)));
    }

}
