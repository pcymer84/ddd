package com.fashionhouse.warehouse.repository;

import com.fashionhouse.core.logger.Log;
import org.springframework.stereotype.Repository;

@Log
@Repository
public interface ParcelRepository extends CrudParcelRepository, CustomParcelRepository {
}
