package com.fashionhouse.warehouse.repository;

import com.fashionhouse.core.logger.Log;
import com.fashionhouse.warehouse.model.Item;
import java.util.Map;
import java.util.UUID;

@Log
public interface CustomItemRepository {

    Item getById(UUID itemId);

    Map<UUID, Item> getItemsById(Iterable<UUID> ids);

    Iterable<Item> saveAll(Map<UUID, Item> items);

}
