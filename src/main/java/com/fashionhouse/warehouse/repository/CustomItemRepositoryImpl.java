package com.fashionhouse.warehouse.repository;

import com.fashionhouse.warehouse.ex.ItemNotFoundException;
import com.fashionhouse.warehouse.model.Item;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
@Repository
public class CustomItemRepositoryImpl implements CustomItemRepository {

    private final CrudItemRepository itemRepository;

    @Override
    public Item getById(UUID itemId) {
        return itemRepository.findById(itemId).orElseThrow(() -> new ItemNotFoundException(String.format("item with id '%s' not found", itemId)));
    }

    @Override
    public Map<UUID, Item> getItemsById(Iterable<UUID> ids) {
        return itemRepository.findAllById(ids).stream().collect(Collectors.toMap(Item::getId, item -> item));
    }

    @Override
    public Iterable<Item> saveAll(Map<UUID, Item> items) {
        return itemRepository.saveAll(items.values());
    }

}
