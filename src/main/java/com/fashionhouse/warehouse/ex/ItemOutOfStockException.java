package com.fashionhouse.warehouse.ex;

import com.fashionhouse.core.ex.UnprocessableException;

public class ItemOutOfStockException extends UnprocessableException {

    public ItemOutOfStockException(String message) {
        super(message);
    }

}
