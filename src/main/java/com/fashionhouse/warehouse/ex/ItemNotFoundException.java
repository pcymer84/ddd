package com.fashionhouse.warehouse.ex;

import com.fashionhouse.core.ex.NotFoundException;

public class ItemNotFoundException extends NotFoundException {

    public ItemNotFoundException(String message) {
        super(message);
    }

}
