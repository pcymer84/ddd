package com.fashionhouse.warehouse.ex;

import com.fashionhouse.core.ex.NotFoundException;

public class ParcelNotModifiableException extends NotFoundException {

    public ParcelNotModifiableException(String message) {
        super(message);
    }

}
