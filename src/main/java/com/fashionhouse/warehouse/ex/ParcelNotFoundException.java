package com.fashionhouse.warehouse.ex;

import com.fashionhouse.core.ex.NotFoundException;

public class ParcelNotFoundException extends NotFoundException {

    public ParcelNotFoundException(String message) {
        super(message);
    }

}
