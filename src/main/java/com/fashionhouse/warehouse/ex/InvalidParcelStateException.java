package com.fashionhouse.warehouse.ex;

import com.fashionhouse.core.ex.UnprocessableException;

public class InvalidParcelStateException extends UnprocessableException {

    public InvalidParcelStateException(String message) {
        super(message);
    }

}
