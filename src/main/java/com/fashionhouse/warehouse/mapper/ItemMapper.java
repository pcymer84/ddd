package com.fashionhouse.warehouse.mapper;

import com.fashionhouse.warehouse.dto.ItemDescriptionRequest;
import com.fashionhouse.warehouse.dto.ItemLocationRequest;
import com.fashionhouse.warehouse.dto.ItemMeasurementRequest;
import com.fashionhouse.warehouse.dto.ItemResponse;
import com.fashionhouse.warehouse.model.ItemDescription;
import com.fashionhouse.warehouse.model.Item;
import com.fashionhouse.warehouse.model.Location;
import com.fashionhouse.warehouse.model.Measurement;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface ItemMapper {

    Measurement mapToMeasurement(ItemMeasurementRequest request);

    Location mapToLocation(ItemLocationRequest request);

    ItemDescription mapToDescription(ItemDescriptionRequest request);

    @Mappings({
            @Mapping(source = "description.name", target = "name"),
            @Mapping(source = "description.comment", target = "comment"),
    })
    ItemResponse mapToItemResponse(Item item);

}
