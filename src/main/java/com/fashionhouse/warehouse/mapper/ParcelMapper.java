package com.fashionhouse.warehouse.mapper;

import com.fashionhouse.warehouse.dto.ParcelEventResponse;
import com.fashionhouse.warehouse.dto.ParcelItemResponse;
import com.fashionhouse.warehouse.dto.ParcelMeasurementRequest;
import com.fashionhouse.warehouse.dto.ParcelResponse;
import com.fashionhouse.warehouse.model.Parcel;
import com.fashionhouse.warehouse.model.ParcelItem;
import com.fashionhouse.warehouse.model.Measurement;
import com.fashionhouse.warehouse.model.event.ParcelStatusEvent;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface ParcelMapper {

    @Mappings({
            @Mapping(source = "parcelItems", target = "items"),
            @Mapping(source = "statusEvents", target = "events")
    })
    ParcelResponse mapToParcelResponse(Parcel parcel);

    Measurement mapToMeasurement(ParcelMeasurementRequest request);

    @Mappings({
            @Mapping(source = "item.name", target = "name"),
            @Mapping(source = "item.comment", target = "comment")
    })
    ParcelItemResponse mapToParcelItemResponse(ParcelItem parcelItem);

    ParcelEventResponse mapToParcelEventResponse(ParcelStatusEvent parcelStatusEvent);

}
