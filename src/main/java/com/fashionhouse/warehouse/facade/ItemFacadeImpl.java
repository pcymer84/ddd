package com.fashionhouse.warehouse.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.core.eventBus.EventPublisher;
import com.fashionhouse.warehouse.dto.ItemDescriptionRequest;
import com.fashionhouse.warehouse.dto.ItemLocationRequest;
import com.fashionhouse.warehouse.dto.ItemMeasurementRequest;
import com.fashionhouse.warehouse.dto.ItemResponse;
import com.fashionhouse.warehouse.event.ItemEventFactory;
import com.fashionhouse.warehouse.mapper.ItemMapper;
import com.fashionhouse.warehouse.model.Item;
import com.fashionhouse.warehouse.model.ItemDescription;
import com.fashionhouse.warehouse.model.Location;
import com.fashionhouse.warehouse.model.Measurement;
import com.fashionhouse.warehouse.repository.ItemRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class ItemFacadeImpl implements ItemFacade {

    private final ItemRepository itemRepository;
    private final ItemMapper itemMapper;
    private final ItemEventFactory itemEventFactory;
    private final EventPublisher eventPublisher;

    @Override
    public ItemResponse updateItemLocation(User user, UUID itemId, ItemLocationRequest request) {
        user.authorize(Permission.ITEM_MANAGER);
        Item item = itemRepository.getById(itemId);
        Location location = itemMapper.mapToLocation(request);
        item.changeLocation(location);
        itemRepository.save(item);
        return itemMapper.mapToItemResponse(item);
    }

    @Override
    public ItemResponse updateItemDescription(User user, UUID itemId, ItemDescriptionRequest request) {
        user.authorize(Permission.ITEM_MANAGER);
        Item item = itemRepository.getById(itemId);
        ItemDescription description = itemMapper.mapToDescription(request);
        item.changeDescription(description);
        itemRepository.save(item);
        return itemMapper.mapToItemResponse(item);
    }

    @Override
    public ItemResponse updateItemMeasurement(User user, UUID itemId, ItemMeasurementRequest request) {
        user.authorize(Permission.ITEM_MANAGER);
        Item item = itemRepository.getById(itemId);
        Measurement description = itemMapper.mapToMeasurement(request);
        item.changeMeasurement(description);
        itemRepository.save(item);
        return itemMapper.mapToItemResponse(item);
    }

    @Override
    public ItemResponse itemsSupply(User user, UUID itemId, Integer amount) {
        user.authorize(Permission.ITEM_SUPPLIER);
        Item item = itemRepository.getById(itemId);
        item.supply(amount);
        itemRepository.save(item);
        eventPublisher.publishEvent(itemEventFactory.createSupplyEvent(item, amount));
        return itemMapper.mapToItemResponse(item);
    }

}
