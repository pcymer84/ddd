package com.fashionhouse.warehouse.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.warehouse.dto.ParcelMeasurementRequest;
import com.fashionhouse.warehouse.dto.ParcelResponse;
import java.util.UUID;

public interface ParcelFacade {

    ParcelResponse packedParcel(User user, UUID parcelId, ParcelMeasurementRequest request);

    ParcelResponse sendParcel(User user, UUID parcelId);

    ParcelResponse returnedParcel(User user, UUID parcelId);

}
