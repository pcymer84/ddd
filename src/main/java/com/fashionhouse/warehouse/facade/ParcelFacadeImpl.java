package com.fashionhouse.warehouse.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.core.eventBus.EventPublisher;
import com.fashionhouse.warehouse.dto.ParcelMeasurementRequest;
import com.fashionhouse.warehouse.dto.ParcelResponse;
import com.fashionhouse.warehouse.event.ParcelEventFactory;
import com.fashionhouse.warehouse.mapper.ParcelMapper;
import com.fashionhouse.warehouse.model.Measurement;
import com.fashionhouse.warehouse.model.Parcel;
import com.fashionhouse.warehouse.repository.ParcelRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class ParcelFacadeImpl implements ParcelFacade {

    private final ParcelRepository parcelRepository;
    private final ParcelMapper parcelMapper;
    private final ParcelEventFactory parcelEventFactory;
    private final EventPublisher eventPublisher;

    @Override
    public ParcelResponse packedParcel(User user, UUID parcelId, ParcelMeasurementRequest request) {
        user.authorize(Permission.PARCEL_SENDER);
        Parcel parcel = parcelRepository.getById(parcelId);
        Measurement measurement = parcelMapper.mapToMeasurement(request);
        parcel.packed(measurement);
        parcelRepository.save(parcel);
        eventPublisher.publishEvent(parcelEventFactory.createParcelPackedEvent(parcel));
        return parcelMapper.mapToParcelResponse(parcel);
    }

    @Override
    public ParcelResponse sendParcel(User user, UUID parcelId) {
        user.authorize(Permission.PARCEL_SENDER);
        Parcel parcel = parcelRepository.getById(parcelId);
        parcel.sent();
        parcelRepository.save(parcel);
        eventPublisher.publishEvent(parcelEventFactory.createParcelSentEvent(parcel));
        return parcelMapper.mapToParcelResponse(parcel);
    }

    @Override
    public ParcelResponse returnedParcel(User user, UUID parcelId) {
        user.authorize(Permission.PARCEL_SENDER);
        Parcel parcel = parcelRepository.getById(parcelId);
        parcel.returned();
        parcelRepository.save(parcel);
        eventPublisher.publishEvent(parcelEventFactory.createParcelReturnedEvent(parcel));
        return parcelMapper.mapToParcelResponse(parcel);
    }

}
