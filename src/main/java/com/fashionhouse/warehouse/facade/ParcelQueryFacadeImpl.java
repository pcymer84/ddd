package com.fashionhouse.warehouse.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.warehouse.dto.ParcelResponse;
import com.fashionhouse.warehouse.mapper.ParcelMapper;
import com.fashionhouse.warehouse.repository.ParcelRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationService
@AllArgsConstructor
public class ParcelQueryFacadeImpl implements ParcelQueryFacade {

    private final ParcelRepository parcelRepository;
    private final ParcelMapper parcelMapper;

    @Override
    public List<ParcelResponse> getParcels(User user) {
        user.authorize(Permission.SHOW_PARCEL);
        return parcelRepository.findAll().stream()
                .map(parcelMapper::mapToParcelResponse)
                .collect(Collectors.toList());
    }

    @Override
    public ParcelResponse getParcel(User user, UUID parcelId) {
        user.authorize(Permission.SHOW_PARCEL);
        return parcelMapper.mapToParcelResponse(parcelRepository.getById(parcelId));
    }

}
