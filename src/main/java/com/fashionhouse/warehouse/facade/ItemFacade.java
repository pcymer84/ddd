package com.fashionhouse.warehouse.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.warehouse.dto.ItemDescriptionRequest;
import com.fashionhouse.warehouse.dto.ItemLocationRequest;
import com.fashionhouse.warehouse.dto.ItemMeasurementRequest;
import com.fashionhouse.warehouse.dto.ItemResponse;
import java.util.UUID;

public interface ItemFacade {

    ItemResponse updateItemLocation(User user, UUID commodityId, ItemLocationRequest request);

    ItemResponse updateItemDescription(User user, UUID itemId, ItemDescriptionRequest request);

    ItemResponse updateItemMeasurement(User user, UUID itemId, ItemMeasurementRequest request);

    ItemResponse itemsSupply(User user, UUID commodityId, Integer amount);

}
