package com.fashionhouse.warehouse.facade;

import com.fashionhouse.core.annotations.ApplicationService;
import com.fashionhouse.core.authentication.model.Permission;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.warehouse.dto.ItemResponse;
import com.fashionhouse.warehouse.mapper.ItemMapper;
import com.fashionhouse.warehouse.repository.ItemRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationService
@AllArgsConstructor
public class ItemQueryFacadeImpl implements ItemQueryFacade {

    private final ItemRepository itemRepository;
    private final ItemMapper itemMapper;

    @Override
    public List<ItemResponse> getItems(User user) {
        user.authorize(Permission.SHOW_ITEM);
        return itemRepository.findAll().stream()
                .map(itemMapper::mapToItemResponse)
                .collect(Collectors.toList());
    }

    @Override
    public ItemResponse getItem(User user, UUID itemId) {
        user.authorize(Permission.SHOW_ITEM);
        return itemMapper.mapToItemResponse(itemRepository.getById(itemId));
    }

}
