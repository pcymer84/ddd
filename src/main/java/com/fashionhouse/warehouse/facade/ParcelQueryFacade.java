package com.fashionhouse.warehouse.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.warehouse.dto.ParcelResponse;
import java.util.List;
import java.util.UUID;

public interface ParcelQueryFacade {

    List<ParcelResponse> getParcels(User user);

    ParcelResponse getParcel(User user, UUID parcelId);

}
