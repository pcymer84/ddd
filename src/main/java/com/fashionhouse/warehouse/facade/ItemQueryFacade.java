package com.fashionhouse.warehouse.facade;

import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.warehouse.dto.ItemResponse;
import java.util.List;
import java.util.UUID;

public interface ItemQueryFacade {

    List<ItemResponse> getItems(User user);

    ItemResponse getItem(User user, UUID commodityId);

}
