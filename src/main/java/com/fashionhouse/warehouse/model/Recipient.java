package com.fashionhouse.warehouse.model;

import com.fashionhouse.core.annotations.ValueObject;
import com.fashionhouse.core.validator.ValidationUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Recipient {

    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    private String phone;
    private String email;

    private Recipient(Recipient.RecipientBuilder builder) {
        firstName = builder.firstName;
        lastName = builder.lastName;
        phone = builder.phone;
        email = builder.email;
        ValidationUtil.validate(this);
    }

    public static class RecipientBuilder {

        public Recipient build() {
            return new Recipient(this);
        }

    }

}
