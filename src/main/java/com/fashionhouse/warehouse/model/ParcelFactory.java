package com.fashionhouse.warehouse.model;

import com.fashionhouse.core.annotations.Factory;
import com.fashionhouse.sales.event.OrderProductDetails;
import com.fashionhouse.sales.event.SubmitOrderEvent;
import com.fashionhouse.warehouse.model.event.ParcelStatusEvent;
import com.google.common.collect.Sets;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Factory
public class ParcelFactory {

    public Parcel createParcel(SubmitOrderEvent event, Map<UUID, Item> items) {
        return Parcel.builder()
                .orderId(event.getOrderId())
                .recipientId(event.getBuyerId())
                .recipient(createRecipient(event))
                .destination(createDestination(event))
                .parcelItems(createParcelItems(event, items))
                .events(Sets.newHashSet(new ParcelStatusEvent(ParcelStatus.AWAIT)))
                .build();
    }

    private Recipient createRecipient(SubmitOrderEvent event) {
        return Recipient.builder()
                .email(event.getRecipient().getEmail())
                .firstName(event.getRecipient().getFirstName())
                .lastName(event.getRecipient().getFirstName())
                .phone(event.getRecipient().getPhone())
                .build();
    }

    private Destination createDestination(SubmitOrderEvent event) {
        return Destination.builder()
                .postcode(event.getDelivery().getPostcode())
                .city(event.getDelivery().getCity())
                .street(event.getDelivery().getStreet())
                .description(event.getDelivery().getDescription())
                .build();
    }

    private Set<ParcelItem> createParcelItems(SubmitOrderEvent event, Map<UUID, Item> items) {
        return event.getOrderProducts().stream()
                .map(orderProduct -> createParcelItem(orderProduct, items.get(orderProduct.getProductId())))
                .collect(Collectors.toSet());
    }

    private ParcelItem createParcelItem(OrderProductDetails orderProduct, Item item) {
        item.checkAvailability(orderProduct.getAmount());
        return ParcelItem.builder()
                .itemId(orderProduct.getProductId())
                .item(item.getDescription())
                .amount(orderProduct.getAmount())
                .build();
    }

}
