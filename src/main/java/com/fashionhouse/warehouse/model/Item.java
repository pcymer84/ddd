package com.fashionhouse.warehouse.model;

import com.fashionhouse.core.annotations.AggregateRoot;
import com.fashionhouse.core.validator.ValidationUtil;
import com.fashionhouse.warehouse.ex.ItemOutOfStockException;
import com.fashionhouse.warehouse.model.event.ItemStockDrawEvent;
import com.fashionhouse.warehouse.model.event.ItemEvent;
import com.fashionhouse.warehouse.model.event.ItemStockEvent;
import com.fashionhouse.warehouse.model.event.ItemStockSupplyEvent;
import com.google.common.collect.Sets;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.CollectionUtils;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@AggregateRoot
@Entity
@Getter
@Builder
@ToString
@Table(name = "items")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Item {

    @Id
    @NotNull
    private UUID id;
    @Embedded
    private ItemDescription description;
    @Embedded
    private Location location;
    @Embedded
    private Measurement measurement;
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "item_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ItemEvent> events;

    public Integer getStock() {
        return events.stream()
                .filter(this::isStockEvent)
                .sorted(ItemEvent::sortNewest)
                .mapToInt(event -> ((ItemStockEvent) event).calculate())
                .sum();
    }

    private boolean isStockEvent(ItemEvent event) {
        return event instanceof ItemStockEvent;
    }

    public void changeLocation(Location location) {
        this.location = location;
    }

    public void changeDescription(ItemDescription description) {
        this.description = description;
    }

    public void changeMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    public void supply(Integer amount) {
        events.add(new ItemStockSupplyEvent(amount));
    }

    public void draw(Integer amount) {
        checkAvailability(amount);
        events.add(new ItemStockDrawEvent(amount));
    }

    public void checkAvailability(Integer amount) {
        if (!isAvailable(amount)) {
            throw new ItemOutOfStockException(String.format("item '%s' is out of stock", id));
        }
    }

    public boolean isAvailable(Integer amount) {
        return getStock() >= amount;
    }

    private Item(Item.ItemBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        description = builder.description;
        location = builder.location;
        measurement = builder.measurement;
        events = CollectionUtils.isEmpty(builder.events) ? Sets.newHashSet() : Sets.newHashSet(builder.events);
        ValidationUtil.validate(this);
    }

    public static class ItemBuilder {

        public Item build() {
            return new Item(this);
        }

    }

}
