package com.fashionhouse.warehouse.model.event;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@ToString(callSuper = true)
@Entity
@Getter
@NoArgsConstructor
@DiscriminatorValue("ItemStockDraw")
public class ItemStockDrawEvent extends ItemStockEvent {

    private Integer amount;

    public ItemStockDrawEvent(Integer amount) {
        this();
        this.amount = amount;
    }

    public int calculate() {
        return -amount;
    }

}
