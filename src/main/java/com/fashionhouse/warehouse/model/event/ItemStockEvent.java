package com.fashionhouse.warehouse.model.event;

import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(callSuper = true)
@NoArgsConstructor
public abstract class ItemStockEvent extends ItemEvent {

    public abstract int calculate();

}
