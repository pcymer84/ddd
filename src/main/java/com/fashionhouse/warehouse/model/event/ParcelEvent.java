package com.fashionhouse.warehouse.model.event;

import com.fashionhouse.core.util.DateTimeProvider;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@ToString
@Getter
@EqualsAndHashCode(of = "id")
@Entity(name = "parcel_events")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class ParcelEvent {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private LocalDateTime date;

    ParcelEvent() {
        this.id = UUID.randomUUID();
        this.date = DateTimeProvider.currentDateTime();
    }

    public static int sortNewest(ParcelEvent event1, ParcelEvent event2) {
        return event2.getDate().compareTo(event1.getDate());
    }

}
