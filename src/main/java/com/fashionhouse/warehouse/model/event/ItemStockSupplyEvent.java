package com.fashionhouse.warehouse.model.event;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@ToString(callSuper = true)
@Entity
@NoArgsConstructor
@DiscriminatorValue("ItemStockSupply")
public class ItemStockSupplyEvent extends ItemStockEvent {

    private Integer amount;

    public ItemStockSupplyEvent(Integer amount) {
        this();
        this.amount = amount;
    }

    public int calculate() {
        return amount;
    }

}
