package com.fashionhouse.warehouse.model.event;

import com.fashionhouse.warehouse.model.ParcelStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@ToString(callSuper = true)
@Entity
@Getter
@NoArgsConstructor
@DiscriminatorValue("ParcelStatus")
public class ParcelStatusEvent extends ParcelEvent {

    @Enumerated(EnumType.STRING)
    private ParcelStatus status;

    public ParcelStatusEvent(ParcelStatus status) {
        this();
        this.status = status;
    }

}
