package com.fashionhouse.warehouse.model;

import com.fashionhouse.core.validator.ValidationUtil;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Getter
@Builder
@ToString
@Table(name="parcel_items")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParcelItem {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private UUID itemId;
    @NotNull
    @Min(1)
    private Integer amount;
    @Embedded
    @NotNull
    private ItemDescription item;

    private ParcelItem(ParcelItem.ParcelItemBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        itemId = builder.itemId;
        amount = builder.amount;
        item = builder.item;
        ValidationUtil.validate(this);
    }

    public static class ParcelItemBuilder {

        public ParcelItem build() {
            return new ParcelItem(this);
        }

    }

}
