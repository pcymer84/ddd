package com.fashionhouse.warehouse.model;

import com.fashionhouse.core.annotations.ValueObject;
import com.fashionhouse.core.validator.ValidationUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ItemDescription {

    @NotNull
    private String name;
    @NotNull
    private String comment;

    private ItemDescription(ItemDescription.ItemDescriptionBuilder builder) {
        name = builder.name;
        comment = builder.comment;
        ValidationUtil.validate(this);
    }

    public static class ItemDescriptionBuilder {

        public ItemDescription build() {
            return new ItemDescription(this);
        }

    }

}
