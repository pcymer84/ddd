package com.fashionhouse.warehouse.model;

import com.fashionhouse.core.annotations.ValueObject;
import com.fashionhouse.core.validator.ValidationUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Location {

    @NotNull
    private String sector;
    @NotNull
    private String shelf;
    @NotNull
    private String bucket;

    private Location(Location.LocationBuilder builder) {
        sector = builder.sector;
        shelf = builder.shelf;
        bucket = builder.bucket;
        ValidationUtil.validate(this);
    }

    public static class LocationBuilder {

        public Location build() {
            return new Location(this);
        }

    }

}
