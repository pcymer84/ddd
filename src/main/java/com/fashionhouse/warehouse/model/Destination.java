package com.fashionhouse.warehouse.model;

import com.fashionhouse.core.annotations.ValueObject;
import com.fashionhouse.core.validator.ValidationUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Destination {

    @NotNull
    private String postcode;
    @NotNull
    private String city;
    @NotNull
    private String street;
    private String description;

    private Destination(Destination.DestinationBuilder builder) {
        postcode = builder.postcode;
        city = builder.city;
        street = builder.street;
        description = builder.description;
        ValidationUtil.validate(this);
    }

    public static class DestinationBuilder {

        public Destination build() {
            return new Destination(this);
        }

    }

}
