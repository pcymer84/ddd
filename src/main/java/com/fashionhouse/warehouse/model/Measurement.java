package com.fashionhouse.warehouse.model;

import com.fashionhouse.core.annotations.ValueObject;
import com.fashionhouse.core.validator.ValidationUtil;
import com.fashionhouse.core.validator.annotations.PositiveNumber;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Embeddable;

@ValueObject
@ToString
@Getter
@Builder
@Embeddable
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Measurement {

    @PositiveNumber
    private Double length;
    @PositiveNumber
    private Double width;
    @PositiveNumber
    private Double height;
    @PositiveNumber
    private Double weight;

    private Measurement(Measurement.MeasurementBuilder builder) {
        length = builder.length;
        width = builder.width;
        height = builder.height;
        weight = builder.weight;
        ValidationUtil.validate(this);
    }

    public static class MeasurementBuilder {

        public Measurement build() {
            return new Measurement(this);
        }

    }

}
