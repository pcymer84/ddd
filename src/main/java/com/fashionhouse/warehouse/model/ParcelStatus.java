package com.fashionhouse.warehouse.model;

public enum ParcelStatus {

    RETURNED,
    CANCELLED,
    AWAIT,
    PACKED,
    SENT,
    DELIVERED

}
