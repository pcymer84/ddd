package com.fashionhouse.warehouse.model;

import com.fashionhouse.core.annotations.AggregateRoot;
import com.fashionhouse.core.validator.ValidationUtil;
import com.fashionhouse.warehouse.ex.InvalidParcelStateException;
import com.fashionhouse.warehouse.ex.ParcelNotModifiableException;
import com.fashionhouse.warehouse.model.event.ParcelEvent;
import com.fashionhouse.warehouse.model.event.ParcelStatusEvent;
import com.google.common.collect.Sets;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.CollectionUtils;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@AggregateRoot
@Entity
@Getter
@Builder
@ToString
@Table(name = "parcels")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Parcel {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private UUID orderId;
    @NotNull
    private UUID recipientId;
    @Embedded
    @NotNull
    private Recipient recipient;
    @Embedded
    private Invoice invoice;
    @Embedded
    @NotNull
    private Destination destination;
    @Embedded
    private Measurement measurement;
    @NotEmpty
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "parcel_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ParcelItem> parcelItems;
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "parcel_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ParcelEvent> events;

    public List<ParcelStatusEvent> getStatusEvents() {
        return events.stream()
                .filter(this::isStatusEvent)
                .sorted(ParcelEvent::sortNewest)
                .map(event -> ((ParcelStatusEvent) event))
                .collect(Collectors.toList());
    }

    private boolean isStatusEvent(ParcelEvent event) {
        return event instanceof ParcelStatusEvent;
    }

    public Set<UUID> getParcelItemIds() {
        return parcelItems.stream()
                .map(ParcelItem::getItemId)
                .collect(Collectors.toSet());
    }

    public Set<ParcelItem> getParcelItems() {
        return Sets.newHashSet(parcelItems);
    }

    public void changeDestination(Destination destination) {
        validateParcelModifiable();
        this.destination = destination;
        ValidationUtil.validate(this);
    }

    public void changeInvoice(Invoice invoice) {
        validateParcelModifiable();
        this.invoice = invoice;
        ValidationUtil.validate(this);
    }

    public void cancel() {
        validateNotCanceled();
        events.add(new ParcelStatusEvent(ParcelStatus.CANCELLED));
    }

    public void packed(Measurement measurement) {
        validateParcelNotFinished();
        this.measurement = measurement;
        events.add(new ParcelStatusEvent(ParcelStatus.PACKED));
    }

    public void sent() {
        validateParcelNotFinished();
        events.add(new ParcelStatusEvent(ParcelStatus.SENT));
    }

    public void delivered() {
        validateParcelNotFinished();
        events.add(new ParcelStatusEvent(ParcelStatus.DELIVERED));
    }

    public void returned() {
        validateParcelNotFinished();
        events.add(new ParcelStatusEvent(ParcelStatus.RETURNED));
    }

    private void validateParcelNotFinished() {
        if (isFinished()) {
            throw new InvalidParcelStateException("parcel is finished");
        }
    }

    private void validateNotCanceled() {
        if (isCanceled()) {
            throw new InvalidParcelStateException(String.format("parcel '%s' is canceled", id) );
        }
    }

    private void validateParcelModifiable() {
        if (!isParcelModifiable()) {
            throw new ParcelNotModifiableException(String.format("parcel '%s' is unmodifiable", id));
        }
    }

    private boolean isParcelModifiable() {
        ParcelStatus currentStatus = getCurrentStatus();
        return currentStatus == ParcelStatus.AWAIT || currentStatus == ParcelStatus.PACKED;
    }

    private boolean isCanceled() {
        return getCurrentStatus() == ParcelStatus.CANCELLED;
    }

    private boolean isFinished() {
        ParcelStatus currentStatus = getCurrentStatus();
        return currentStatus == ParcelStatus.CANCELLED || currentStatus == ParcelStatus.DELIVERED;
    }

    private ParcelStatus getCurrentStatus() {
        return getStatusEvents().stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("parcel '%s' without status", id)))
                .getStatus();
    }

    private Parcel(Parcel.ParcelBuilder builder) {
        id = builder.id == null ? UUID.randomUUID() : builder.id;
        orderId = builder.orderId;
        recipientId = builder.recipientId;
        recipient = builder.recipient;
        measurement = builder.measurement;
        destination = builder.destination;
        parcelItems = CollectionUtils.isEmpty(builder.parcelItems) ? Sets.newHashSet() : Sets.newHashSet(builder.parcelItems);
        events = CollectionUtils.isEmpty(builder.events) ? Sets.newHashSet() : Sets.newHashSet(builder.events);
        ValidationUtil.validate(this);
    }

    public static class ParcelBuilder {

        public Parcel build() {
            return new Parcel(this);
        }

    }

}
