package com.fashionhouse.warehouse.service;

import com.fashionhouse.core.annotations.DomainService;
import com.fashionhouse.warehouse.model.Item;
import com.fashionhouse.warehouse.model.Parcel;
import lombok.AllArgsConstructor;
import java.util.Map;
import java.util.UUID;

@DomainService
@AllArgsConstructor
public class ParcelItemStockService {

    public void drawItemsToParcel(Parcel parcel, Map<UUID, Item> items) {
        parcel.getParcelItems().forEach(parcelItem -> items.get(parcelItem.getItemId()).draw(parcelItem.getAmount()));
    }

    public void supplyItemsFromParcel(Parcel parcel, Map<UUID, Item> items) {
        parcel.getParcelItems().forEach(parcelItem -> items.get(parcelItem.getItemId()).supply(parcelItem.getAmount()));
    }

}
