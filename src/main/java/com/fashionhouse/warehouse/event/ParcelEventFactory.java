package com.fashionhouse.warehouse.event;

import com.fashionhouse.warehouse.model.Parcel;
import org.springframework.stereotype.Component;

@Component
public class ParcelEventFactory {

    public ParcelPackedEvent createParcelPackedEvent(Parcel parcel) {
        return ParcelPackedEvent.builder()
                .parcelId(parcel.getId())
                .orderId(parcel.getOrderId())
                .build();
    }

    public ParcelSentEvent createParcelSentEvent(Parcel parcel) {
        return ParcelSentEvent.builder()
                .parcelId(parcel.getId())
                .orderId(parcel.getOrderId())
                .build();
    }

    public ParcelReturnedEvent createParcelReturnedEvent(Parcel parcel) {
        return ParcelReturnedEvent.builder()
                .parcelId(parcel.getId())
                .orderId(parcel.getOrderId())
                .build();
    }

}
