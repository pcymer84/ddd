package com.fashionhouse.warehouse.event;

import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.Value;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Value
@ToString
@AllArgsConstructor
public class ItemDrawEvent {

    private List<ItemDrawDetails> draws;

    public List<UUID> getItemIds() {
        return draws.stream()
                .map(ItemDrawDetails::getItemId)
                .collect(Collectors.toList());
    }

}
