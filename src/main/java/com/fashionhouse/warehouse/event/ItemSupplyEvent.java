package com.fashionhouse.warehouse.event;

import lombok.ToString;
import lombok.Value;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ToString
@Value
public class ItemSupplyEvent {

    private List<ItemSupplyDetails> supplies;

    public List<UUID> getItemIds() {
        return supplies.stream()
                .map(ItemSupplyDetails::getItemId)
                .collect(Collectors.toList());
    }

}
