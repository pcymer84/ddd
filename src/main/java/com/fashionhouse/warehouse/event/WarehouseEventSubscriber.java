package com.fashionhouse.warehouse.event;

import com.fashionhouse.core.eventBus.EventPublisher;
import com.fashionhouse.core.eventBus.EventSubscriber;
import com.fashionhouse.core.logger.Log;
import com.fashionhouse.design.event.DesignApprovedEvent;
import com.fashionhouse.sales.event.DeliveredOrderEvent;
import com.fashionhouse.sales.event.OrderDeliveryChangeEvent;
import com.fashionhouse.sales.event.OrderInvoiceChangeEvent;
import com.fashionhouse.sales.event.RefundOrderEvent;
import com.fashionhouse.sales.event.SubmitOrderEvent;
import com.fashionhouse.warehouse.ex.ItemOutOfStockException;
import com.fashionhouse.warehouse.model.Destination;
import com.fashionhouse.warehouse.model.Invoice;
import com.fashionhouse.warehouse.model.Item;
import com.fashionhouse.warehouse.model.ItemDescription;
import com.fashionhouse.warehouse.model.Parcel;
import com.fashionhouse.warehouse.model.ParcelFactory;
import com.fashionhouse.warehouse.repository.ItemRepository;
import com.fashionhouse.warehouse.repository.ParcelRepository;
import com.fashionhouse.warehouse.service.ParcelItemStockService;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;

import java.util.Map;
import java.util.UUID;

@Log
@EventSubscriber
@AllArgsConstructor
public class WarehouseEventSubscriber {

    private final ItemRepository itemRepository;
    private final ParcelRepository parcelRepository;
    private final ParcelFactory parcelFactory;
    private final ParcelItemStockService parcelItemStockService;
    private final ItemEventFactory itemEventFactory;
    private final EventPublisher eventPublisher;

    @Subscribe
    public void onDesignApprovedEvent(DesignApprovedEvent event) {
        itemRepository.save(createItem(event));
    }

    @Subscribe
    public void onRefundOrderEvent(RefundOrderEvent event) {
        Parcel parcel = parcelRepository.getByOrderId(event.getOrderId());
        parcel.cancel();
        Map<UUID, Item> items = itemRepository.getItemsById(parcel.getParcelItemIds());
        parcelItemStockService.supplyItemsFromParcel(parcel, items);
        parcelRepository.save(parcel);
        itemRepository.saveAll(items);
        eventPublisher.publishEvent(itemEventFactory.createSupplyEvent(parcel, items));
    }

    @Subscribe
    public void onDeliveredOrderEvent(DeliveredOrderEvent event) {
        Parcel parcel = parcelRepository.getByOrderId(event.getOrderId());
        parcel.delivered();
        parcelRepository.save(parcel);
    }

    @Subscribe
    public void onOrderInvoiceChangeEvent(OrderInvoiceChangeEvent event) {
        Parcel parcel = parcelRepository.getById(event.getOrderId());
        parcel.changeInvoice(createInvoice(event));
        parcelRepository.save(parcel);
    }

    @Subscribe
    public void onOrderDeliveryChangeEvent(OrderDeliveryChangeEvent event) {
        Parcel parcel = parcelRepository.getById(event.getOrderId());
        parcel.changeDestination(createDestination(event));
        parcelRepository.save(parcel);
    }

    @Subscribe
    public void onSubmitOrderEvent(SubmitOrderEvent event) {
        try {
            Map<UUID, Item> items = itemRepository.getItemsById(event.getProductIds());
            Parcel parcel = parcelFactory.createParcel(event, items);
            parcelItemStockService.drawItemsToParcel(parcel, items);
            parcelRepository.save(parcel);
            itemRepository.saveAll(items);
            eventPublisher.publishEvent(itemEventFactory.createDrawEvent(parcel, items));
        } catch (ItemOutOfStockException e) {
            eventPublisher.publishEvent(new UnrealizableParcelEvent(event.getOrderId()));
            throw e;
        }
    }

    private Invoice createInvoice(OrderInvoiceChangeEvent event) {
        return Invoice.builder()
                .nip(event.getNip())
                .name(event.getName())
                .address(event.getAddress())
                .date(event.getDate())
                .build();
    }

    private Destination createDestination(OrderDeliveryChangeEvent event) {
        return Destination.builder()
                .postcode(event.getPostcode())
                .city(event.getCity())
                .street(event.getStreet())
                .description(event.getDescription())
                .build();
    }

    private Item createItem(DesignApprovedEvent event) {
        return Item.builder()
                .id(event.getDesignId())
                .description(createDescription(event))
                .build();
    }

    private ItemDescription createDescription(DesignApprovedEvent event) {
        return ItemDescription.builder()
                .name(event.getName())
                .comment(event.getDescription())
                .build();
    }

}
