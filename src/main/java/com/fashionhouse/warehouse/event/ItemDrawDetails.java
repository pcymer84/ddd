package com.fashionhouse.warehouse.event;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@Value
@Builder
@ToString
public class ItemDrawDetails {

    private UUID itemId;
    private Integer stock;
    private Integer draw;

}
