package com.fashionhouse.warehouse.event;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@ToString
@Value
@Builder
public class ParcelPackedEvent {

    private UUID parcelId;
    private UUID orderId;

}
