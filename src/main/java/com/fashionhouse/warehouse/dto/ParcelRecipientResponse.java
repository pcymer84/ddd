package com.fashionhouse.warehouse.dto;

import lombok.Data;

@Data
public class ParcelRecipientResponse {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;

}
