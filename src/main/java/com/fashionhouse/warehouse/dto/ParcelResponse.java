package com.fashionhouse.warehouse.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParcelResponse {

    private UUID id;
    private UUID orderId;
    private UUID recipientId;
    private ParcelDestinationResponse destination;
    private ParcelMeasurementResponse measurement;
    private ParcelRecipientResponse recipient;
    private List<ParcelItemResponse> items;
    private List<ParcelEventResponse> events;

}
