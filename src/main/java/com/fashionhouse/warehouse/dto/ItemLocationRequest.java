package com.fashionhouse.warehouse.dto;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class ItemLocationRequest {

    @NotNull
    private String sector;
    @NotNull
    private String shelf;
    @NotNull
    private String bucket;

}
