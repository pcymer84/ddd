package com.fashionhouse.warehouse.dto;

import com.fashionhouse.warehouse.model.ParcelStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParcelEventResponse {

    private ParcelStatus status;
    private LocalDateTime date;

}
