package com.fashionhouse.warehouse.dto;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class ItemDescriptionRequest {

    @NotNull
    private String name;
    private String description;

}
