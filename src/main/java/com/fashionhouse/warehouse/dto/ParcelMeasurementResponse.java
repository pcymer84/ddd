package com.fashionhouse.warehouse.dto;

import lombok.Data;

@Data
public class ParcelMeasurementResponse {

    private Double length;
    private Double width;
    private Double height;
    private Double weight;

}
