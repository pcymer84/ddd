package com.fashionhouse.warehouse.dto;

import lombok.Data;

@Data
public class ItemLocationResponse {

    private String sector;
    private String shelf;
    private String bucket;

}
