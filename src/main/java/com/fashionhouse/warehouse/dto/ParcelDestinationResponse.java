package com.fashionhouse.warehouse.dto;

import lombok.Data;

@Data
public class ParcelDestinationResponse {

    private String postcode;
    private String city;
    private String street;
    private String description;

}
