package com.fashionhouse.warehouse.dto;

import com.fashionhouse.core.validator.annotations.PositiveNumber;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class ItemMeasurementRequest {

    @NotNull
    @PositiveNumber
    private Double length;
    @NotNull
    @PositiveNumber
    private Double width;
    @NotNull
    @PositiveNumber
    private Double height;
    @NotNull
    @PositiveNumber
    private Double weight;

}
