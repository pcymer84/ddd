package com.fashionhouse.warehouse.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class ItemResponse {

    private UUID id;
    private String name;
    private String comment;
    private Integer stock;
    private ItemLocationResponse location;
    private ItemMeasurementResponse measurement;

}
