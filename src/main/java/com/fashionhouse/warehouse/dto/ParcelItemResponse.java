package com.fashionhouse.warehouse.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParcelItemResponse {

    private UUID id;
    private UUID itemId;
    private Integer amount;
    private String name;
    private String comment;

}
