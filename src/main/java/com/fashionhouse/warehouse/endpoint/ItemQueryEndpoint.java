package com.fashionhouse.warehouse.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.warehouse.dto.ItemResponse;
import com.fashionhouse.warehouse.facade.ItemQueryFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ItemQueryEndpoint {

    private final ItemQueryFacade itemQueryFacade;

    @GetMapping("/items")
    public List<ItemResponse> getItems(@AuthenticationPrincipal User user) {
        return itemQueryFacade.getItems(user);
    }

    @GetMapping("/items/{itemId}")
    public ItemResponse getItem(@AuthenticationPrincipal User user,
                                @PathVariable("itemId") UUID itemId) {
        return itemQueryFacade.getItem(user, itemId);
    }

}
