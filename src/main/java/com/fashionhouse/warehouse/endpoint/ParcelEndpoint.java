package com.fashionhouse.warehouse.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.warehouse.dto.ParcelMeasurementRequest;
import com.fashionhouse.warehouse.dto.ParcelResponse;
import com.fashionhouse.warehouse.facade.ParcelFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ParcelEndpoint {

    private final ParcelFacade parcelFacade;

    @PutMapping("/parcels/{parcelId}/packed")
    public ParcelResponse packedParcel(@AuthenticationPrincipal User user,
                                       @PathVariable("parcelId") UUID parcelId,
                                       @Valid @RequestBody ParcelMeasurementRequest request) {
        return parcelFacade.packedParcel(user, parcelId, request);
    }

    @PutMapping("/parcels/{parcelId}/send")
    public ParcelResponse sendParcel(@AuthenticationPrincipal User user,
                                     @PathVariable("parcelId") UUID parcelId) {
        return parcelFacade.sendParcel(user, parcelId);
    }

    @PutMapping("/parcels/{parcelId}/returned")
    public ParcelResponse returnedParcel(@AuthenticationPrincipal User user,
                                         @PathVariable("parcelId") UUID parcelId) {
        return parcelFacade.returnedParcel(user, parcelId);
    }

}
