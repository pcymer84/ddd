package com.fashionhouse.warehouse.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.warehouse.dto.ParcelResponse;
import com.fashionhouse.warehouse.facade.ParcelQueryFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ParcelQueryEndpoint {

    private final ParcelQueryFacade parcelQueryFacade;

    @GetMapping("/parcels")
    public List<ParcelResponse> getParcels(@AuthenticationPrincipal User user) {
        return parcelQueryFacade.getParcels(user);
    }

    @GetMapping("/parcels/{parcelId}")
    public ParcelResponse getParcel(@AuthenticationPrincipal User user,
                                    @PathVariable("parcelId") UUID parcelId) {
        return parcelQueryFacade.getParcel(user, parcelId);
    }

}
