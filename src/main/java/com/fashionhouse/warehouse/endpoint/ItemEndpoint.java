package com.fashionhouse.warehouse.endpoint;

import com.fashionhouse.core.annotations.ApiV1;
import com.fashionhouse.core.annotations.Endpoint;
import com.fashionhouse.core.authentication.model.User;
import com.fashionhouse.warehouse.dto.ItemDescriptionRequest;
import com.fashionhouse.warehouse.dto.ItemLocationRequest;
import com.fashionhouse.warehouse.dto.ItemMeasurementRequest;
import com.fashionhouse.warehouse.dto.ItemResponse;
import com.fashionhouse.warehouse.facade.ItemFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ItemEndpoint {

    private final ItemFacade itemFacade;

    @PutMapping("/items/{itemId}/description")
    public ItemResponse updateItemDescription(@AuthenticationPrincipal User user,
                                              @PathVariable("itemId") UUID itemId,
                                              @Valid @RequestBody ItemDescriptionRequest request) {
        return itemFacade.updateItemDescription(user, itemId, request);
    }

    @PutMapping("/items/{itemId}/location")
    public ItemResponse updateItemLocation(@AuthenticationPrincipal User user,
                                           @PathVariable("itemId") UUID itemId,
                                           @Valid @RequestBody ItemLocationRequest request) {
        return itemFacade.updateItemLocation(user, itemId, request);
    }

    @PutMapping("/items/{itemId}/measurement")
    public ItemResponse updateItemMeasurement(@AuthenticationPrincipal User user,
                                              @PathVariable("itemId") UUID itemId,
                                              @Valid @RequestBody ItemMeasurementRequest request) {
        return itemFacade.updateItemMeasurement(user, itemId, request);
    }

    @PutMapping("/items/{itemId}/supply/{amount}")
    public ItemResponse itemsSupply(@AuthenticationPrincipal User user,
                                    @PathVariable("itemId") UUID itemId,
                                    @PathVariable Integer amount) {
        return itemFacade.itemsSupply(user, itemId, amount);
    }

}
